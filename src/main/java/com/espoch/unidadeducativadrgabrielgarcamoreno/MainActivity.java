package com.espoch.unidadeducativadrgabrielgarcamoreno;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Menu1.Menu1;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2.menu2;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import ggm.comun.login;
import ggm.comun.usuario;

public class MainActivity extends AppCompatActivity {
    public static final String LOCAL_HOST = "10.0.2.2";
    public static final String PUERTO = "8080";

    private String mensaje;
    private String json;
    private String strToken="";

    private login objetoLogin;
    private usuario objetoUsuario;
    private mainModelo objetoModelo;
    private Gson objetoGson;
    boolean hayConexion;

    public MainActivity(){
        mensaje = "";
        json = "";
        strToken = "";
        objetoLogin = new login();
        objetoUsuario = new usuario();
        objetoModelo = new mainModelo();
        objetoGson = new Gson();
        hayConexion = false;
    }

    private EditText edtCedula;
    private EditText edtContrasena;
    private ProgressBar prgLogin;
    private Intent miIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtCedula = findViewById(R.id.edtCedula);
        edtContrasena = findViewById(R.id.edtContrasena);
        prgLogin = findViewById(R.id.prgLogin);
        //prgLogin.setVisibility(View.INVISIBLE);
    }

//MÉTODO PARA EL LOGEO DE LOS USUARIOS-------------------------------------------------------------
    public void login (View v){
        objetoLogin.setStrCedula(edtCedula.getText().toString());
        objetoLogin.setStrContrasena(edtContrasena.getText().toString());
        json = objetoGson.toJson(objetoLogin);
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        hayConexion = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        if (hayConexion){
            MyTask mt = new MyTask();
            mt.execute();//Llama a la tarea Asíncrona para el login---------------------------------
            getTokenActual();//Obtiene el token actual--------------------------------------------------
            upToken ut = new upToken();
            ut.execute();//Llama a la tarea Asíncrona para el token---------------------------------

        } else {
            Toast.makeText(getApplicationContext(), "No tiene acceso a internet", Toast.LENGTH_LONG).show();
            //***************************************************************
            miIntent  = new Intent(MainActivity.this, Menu1.class);
            startActivity(miIntent);
            //****************************************************************
        }
    }

    //Métodos para la obtención y el guardado del token, dado que es funcion void-------------------
    public void getTokenActual(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(!task.isSuccessful()){
                            return;
                        }
                        String too = task.getResult().getToken();
                        guardarToken(too);
                    }
                });
    }
    public void guardarToken(String newToken){
        strToken = newToken;
        return;
    }


//MÉTODOS PARA LA CONEXIÓN CON LOS SERVICIOS WEB----------------------------------------------------
    class MyTask extends AsyncTask {
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        prgLogin.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
        prgLogin.setProgress((int)values[0]);
    }

    @Override
        protected Object doInBackground(Object[] objects){
            mensaje = objetoModelo.loadData(json);
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            prgLogin.setVisibility(View.INVISIBLE);
            if(!mensaje.equals("-2") && !mensaje.equals("-1") && !mensaje.equals("-3")){
                objetoUsuario = objetoGson.fromJson(mensaje, usuario.class);
                Toast.makeText(getApplicationContext(), "Bienvenido: "+ objetoUsuario.getStrNombre()+" "+ objetoUsuario.getStrApellido(), Toast.LENGTH_LONG).show();
                if(objetoUsuario.getStrTipo().equals("Representante")){
                    miIntent  = new Intent(MainActivity.this, Menu1.class);
                }else  if(objetoUsuario.getStrTipo().equals("Docente")){
                    miIntent  = new Intent(MainActivity.this, menu2.class);
                }

                Bundle oBundle = new Bundle();
                oBundle.putString("json", mensaje);
                miIntent.putExtras(oBundle);
                startActivity(miIntent);
            } else if(mensaje.equals("-1")){
                Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_LONG).show();
            } else {Toast.makeText(getApplicationContext(), "Servidor desconectado", Toast.LENGTH_LONG).show();}
            super.onPostExecute(o);
        }
    }

//CLASE PARA SUBIR O ACTUALIZAR EL TOKEN----------------------------------------------------------
    class upToken extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                mensaje = objetoModelo.loadToken(json, strToken);
            }
            return null;
        }
        @Override
        protected void onPostExecute(Object o){ }
    }
}
