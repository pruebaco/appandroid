package com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores.adaptadorPerfilesEstudiantes;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.EstudianteMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.estudiante;

public class opcionesPerfilesEstudiantes extends AppCompatActivity {
    private Bundle oBundle;
    private String json = "";
    private String mensaje = "";
    ArrayList<estudiante> listaDatos;
    estudiante oEstudiante = new estudiante();
    Gson oGson = new Gson();
    List<estudiante> listaEstudiantes;

    EstudianteMO oMOPE = new EstudianteMO();
    Intent myIntent;

    RecyclerView recycler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcs_perfil_estudiante);

        recycler = findViewById(R.id.rcvPerfilesEstudiantes);
        listaDatos= new ArrayList<>();
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        recycler.setHasFixedSize(true);


        //Parámetro recibido del anterior activity--------------------------------------------------
        oBundle = this.getIntent().getExtras();
        if(oBundle!= null){
            json = oBundle.getString("json");
            miLista ml = new miLista();
            ml.execute();
        }
    }



//CLASE PARA HACER LA CONEXIÓN CON EL SERVICIO WEB LISTAR ESTUDIANTES POR REPRESENTANTE------------
class miLista extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMOPE.listarEstudiantesDeRepresentante(json);
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                listaEstudiantes = oGson.fromJson(mensaje, new TypeToken<List<estudiante>>(){}.getType());
                if (listaEstudiantes!= null){
                    llenarPersonajes();

                    adaptadorPerfilesEstudiantes aPE = new adaptadorPerfilesEstudiantes(listaDatos);
                    aPE.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Toast.makeText(getApplicationContext(),
//                                    "Seleccion: "+listaDatos.get
//                                    (recycler.getChildAdapterPosition(v)).getStrNombres(),
//                                    Toast.LENGTH_SHORT).show();

                            oEstudiante.setStrNombres(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrNombres());
                            oEstudiante.setStrApellidos(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrApellidos());
                            oEstudiante.setStrCedula(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrCedula());
                            oEstudiante.setIntEdad(listaDatos.get(recycler.getChildAdapterPosition(v)).getIntEdad());
                            //oEstudiante.setStrFoto(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrFoto());

                            json = oGson.toJson(oEstudiante);
                            System.out.println(json);
                            myIntent  = new Intent(opcionesPerfilesEstudiantes.this, perfilEstudiante.class);
                            oBundle = new Bundle();
                            oBundle.putString("json", json);
                            myIntent.putExtras(oBundle);
                            startActivity(myIntent);
                            finish();
                        }
                    });
                    recycler.setAdapter(aPE);
                }

            }
        }
}


    private void llenarPersonajes() {
        for(estudiante object : listaEstudiantes){
            listaDatos.add(object);
        }



        oEstudiante.setStrNombres("Juan Gabriel");
        oEstudiante.setStrApellidos("Perez Gomez Chanta Coro");
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
//        listaDatos.add(oEstudiante);
    }
}
