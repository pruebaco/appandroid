package com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

//import com.espoch.unidadeducativadrgabrielgarcamoreno.ItemAdapter;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.EstudianteMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.espoch.unidadeducativadrgabrielgarcamoreno.RecyclerItemClickListener;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores.adaptadorListadoEstudiantesParalelo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
//import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.estudiante;

public class listadoEstudiantesDeParalelo extends AppCompatActivity implements ActionMode.Callback {

    private boolean isMultiSelect = false;
    private List<Integer> selectedIds = new ArrayList<>();
    private adaptadorListadoEstudiantesParalelo adapter;
    private Toolbar oToolbar;
    private Bundle objetoBundle;
    private EstudianteMO oMOPE = new EstudianteMO();
    private String mensaje = "";
    private String json = "";
    private String jsonActividad = "";
    private String jsonParalelo = "";
    private String jsonListado = "";
    private List<estudiante> listaEstudiantes = new ArrayList<>();
    private List<estudiante> listaDatos = new ArrayList<>();
    private Gson oGson = new Gson();
    private ActionMode actionMode;
    private boolean estado;
    RecyclerView auxrecyclerView;


    /*
    * EL ONCREATE DE LA ACTIVIDAD-------------------------------------------------------------------
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_paralelos);

        objetoBundle = this.getIntent().getExtras();
        if(objetoBundle!= null){
            json = objetoBundle.getString("jsonClase");
            jsonActividad = objetoBundle.getString("jsonActividad");
            jsonParalelo = objetoBundle.getString("jsonParalelo");

            Toast.makeText(this, "Selected items are :" +json+""+jsonParalelo, Toast.LENGTH_SHORT).show();
            claseListaEstudiantes cLE = new claseListaEstudiantes();
            cLE.execute();
        }

        final RecyclerView finalrecyclerView = findViewById(R.id.rcView);
        finalrecyclerView.setLayoutManager(new LinearLayoutManager(this));
        auxrecyclerView = finalrecyclerView;

        oToolbar = findViewById(R.id.tbrModificar);
        oToolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Estudiantes destinatarios</font>"));
        setSupportActionBar(oToolbar);
        oToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);

        /*
        * Cuando se encuentra ya en action mode-----------------------------------------------------
        * */
        finalrecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, finalrecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isMultiSelect){
                    //if multiple selection is enabled then select item on single click else perform normal click on item.
                    multiSelect(position);
                }
            }

        /*
        * Se empieza el action mode mediante la selección sostenida---------------------------------
        * */
            @Override
            public void onItemLongClick(View view, int position) {
                if (!isMultiSelect){
                    selectedIds = new ArrayList<>();
                    isMultiSelect = true;
                    if (actionMode == null){
                        getSupportActionBar().hide();
                        actionMode = startActionMode(listadoEstudiantesDeParalelo.this); //show ActionMode.
                        finalrecyclerView.setPadding(0,0,0,16);
                    }
                }
                System.out.println("Estamos en la posición: "+ position);
                multiSelect(position);
            }
        }));
    }

    /*
     * CLASE PARA CONSULTA DE ESTUDIANTES DE UN PARALELO--------------------------------------------
     * */
    class claseListaEstudiantes extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMOPE.listarEstudiantesDeParalelo(jsonParalelo);
            estado = true;
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                listaEstudiantes = oGson.fromJson(mensaje, new TypeToken<List<estudiante>>(){}.getType());
                adaptadorListadoEstudiantesParalelo adapter2 = new adaptadorListadoEstudiantesParalelo(listadoEstudiantesDeParalelo.this, getList());
                adapter = adapter2;
                auxrecyclerView.setAdapter(adapter);
                //prueba(mensaje);

            }
        }
    }

    /*
     * CLASE PARA ENVIAR LA ACTIVIDAD CON EL LISTADO DE ESTUDIANTES---------------------------------
     * */
    class claseEnviarActividad extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMOPE.enviarActividadEstudiantes(json, jsonActividad, jsonListado);
            estado = true;
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                System.out.println("JSON DE RESPUESTA "+mensaje);

            }
        }
    }


    /*
    * Método para mostrar contenido en el toolbar del action mode-----------------------------------
    * */
    private void multiSelect(int position) {
        estudiante data = adapter.getItem(position);
        if (data != null){
            if (actionMode != null) {
                if (selectedIds.contains(data.getIntIdEstudiante()))
                    selectedIds.remove(Integer.valueOf(data.getIntIdEstudiante()));
                else
                    selectedIds.add(data.getIntIdEstudiante());
                if (selectedIds.size() > 0)
                    actionMode.setTitle(String.valueOf(selectedIds.size())+" Estudiante(s) seleccionado(s)"); //show selected item count on action mode.
                else{
                    actionMode.setTitle(""); //remove item count from action mode.
                    actionMode.finish(); //hide action mode.
                }
                adapter.setSelectedIds(selectedIds);
            }
        }
    }

    private void multiSelect2(int position) {
        estudiante data = adapter.getItem(position);
        if (data != null){
            if (actionMode != null) {
                if (selectedIds.size() >= 0)
                    actionMode.setTitle(String.valueOf(selectedIds.size()+1)+" Estudiante(s) seleccionado(s)"); //show selected item count on action mode.
                else{
                    actionMode.setTitle(""); //remove item count from action mode.
                    //actionMode.finish(); //hide action mode.
                }
                adapter.setSelectedIds(selectedIds);
            }
        }
    }

    /*
     * Método que obtiene la lista para mostrar en el recycler view---------------------------------
     * @return list
     */
    private List<estudiante> getList(){
        List<estudiante> listaux = new ArrayList<>();
        if(listaEstudiantes != null){
            for (estudiante oEst : listaEstudiantes){
                listaux.add(oEst);
            }

            return listaux;
        }else{
            System.out.println("AUN NO OBTIENE DATOS");
            return listaux;
        }
    }

    /*
    * Método para mostrar el toolbar del action mode------------------------------------------------
    * */
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_seleccionar_estudiantes, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    /*
    * Método para saber que hace cada opción del toolbar del action mode---------------------------
    * */
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
        switch (menuItem.getItemId()){

            case R.id.opEnviar:
                AlertDialog.Builder alerta = new AlertDialog.Builder(listadoEstudiantesDeParalelo.this);
                alerta.setMessage("¿Desea enviar la actividad a los estudiantes seleccionados?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                StringBuilder stringBuilder = new StringBuilder();
                                JSONObject obj = new JSONObject();
                                JSONArray listAux = new JSONArray();
                                try {
                                    for(int i : selectedIds){
                                        listAux.put(i);
                                    }
                                    obj.put("id", listAux);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                jsonListado = obj.toString();
                                System.out.println("JSON DE LISTADO DE ESTUDIANTES "+jsonListado);
                                claseEnviarActividad cl = new claseEnviarActividad();
                                cl.execute();
                                for (estudiante data : getList()) {
                                    if (selectedIds.contains(data.getIntIdEstudiante()))
                                        stringBuilder.append("\n").append(data.getStrNombres());

                                }
                                Toast.makeText(listadoEstudiantesDeParalelo.this, "Selected items are :" + stringBuilder.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog titulo = alerta.create();
                titulo.setTitle("Confirmación");
                titulo.show();


                return true;

            case R.id.opSeleccionarTodo:
                selectedIds = new ArrayList<>();
                selectedIds.clear();
                isMultiSelect = true;
                StringBuilder stringBuilder2 = new StringBuilder();
                int i = 0;
               // getList();
                for(estudiante dato : getList()){
                    multiSelect2(i);
                    selectedIds.add(dato.getIntIdEstudiante());
                    stringBuilder2.append("\n").append(dato.getStrNombres());
                    i++;
                }
                Toast.makeText(this, "Selected items are :", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }

    /*
    * Acciones al momento de destruir el action mode------------------------------------------------
    * */
    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        isMultiSelect = false;
        selectedIds = new ArrayList<>();
        adapter.setSelectedIds(new ArrayList<Integer>());
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}