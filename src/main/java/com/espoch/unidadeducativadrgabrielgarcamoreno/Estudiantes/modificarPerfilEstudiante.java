package com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.modeloModificarPerfilEstudiante;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import ggm.comun.estudiante;

import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class modificarPerfilEstudiante extends AppCompatActivity {
    private EditText edtNombre;
    private EditText edtApellido;
    private EditText edtCedula;
    private EditText edtEdad;
    private ImageView imgFoto;
    String json = "";
    Bundle oBundle;
    estudiante oEstudiante = new estudiante();
    modeloModificarPerfilEstudiante oMMPE = new modeloModificarPerfilEstudiante();
    Gson oGson = new Gson();
    Bitmap bmp;
    Intent miIntent;
    Uri path;
    Bitmap aux;
    Bitmap bitmap;
    String mensaje ="";
    byte[] oByte;

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_perfil_estudiante);
        edtNombre = findViewById(R.id.edtNombreActividadM);
        edtApellido = findViewById(R.id.edtApellidoEstudianteM);
        edtCedula = findViewById(R.id.edtCedulaEstudianteM);
        edtEdad = findViewById(R.id.edtEdadEstudianteM);
        imgFoto = findViewById(R.id.imgFotoEstudianteM);

        try{
            oBundle = this.getIntent().getExtras();
            if(oBundle!=null){
                json = oBundle.getString("json");
                oEstudiante = oGson.fromJson(json, estudiante.class);
                edtNombre.setText(oEstudiante.getStrNombres());
                edtApellido.setText(oEstudiante.getStrApellidos());
                edtCedula.setText(oEstudiante.getStrCedula());
                edtEdad.setText(oEstudiante.getIntEdad().toString());
                Picasso.with(getApplicationContext()).load("http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/files/"+oEstudiante.getStrCedula()+".jpg").error(R.mipmap.nodisponible).memoryPolicy(MemoryPolicy.NO_CACHE).into(imgFoto);
            }
        }catch(Exception e){

        }
        toolbar = findViewById(R.id.tbrModificar);
        toolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Modificar perfil</font>"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);

    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_modificar_perfil, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem){
        Integer aux = menuItem.getItemId();
        switch (aux){
            case R.id.opGuardar:
                if(oBundle != null){
                    Modificar oM = new Modificar();
                    oM.execute();
                }else {Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();}
                break;

            case android.R.id.home:
                if(oBundle != null){
                    miIntent  = new Intent(modificarPerfilEstudiante.this, perfilEstudiante.class);
                    oBundle = new Bundle();
                    oBundle.putString("json", json);
                    miIntent.putExtras(oBundle);
                    startActivity(miIntent);
                    finish();
                }


                System.out.println("Atras");
                break;
        }

        return true;
    }

//Cargar la imagen en un imageview------------------------------------------------------------------
    public void CargarImagenEstudiante(View view) {
        CargarImagenFuncion();
    }
    private void CargarImagenFuncion() {
        miIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        miIntent.setType("image/");
        startActivityForResult(miIntent.createChooser(miIntent, "Seleccione la aplicacion"), 10);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode ==RESULT_OK){
            path = data.getData();
            try {
                aux = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), path);
                imgFoto.setImageBitmap(aux);
                bitmap = ((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                oByte = baos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //Método para subir la imagen al servidor-----------------------------------------------------------
    public void subirImagenBoton(View v){
        subirImagen sI = new subirImagen();
        sI.execute();
    }
    class subirImagen extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMMPE.subirImagen(oByte, edtCedula.getText().toString());
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2")){
                Toast.makeText(getApplicationContext(), "Datos: "+mensaje, Toast.LENGTH_LONG).show();
            }
        }
    }




    class Modificar extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            oEstudiante.setStrNombres(edtNombre.getText().toString());
            oEstudiante.setStrApellidos(edtApellido.getText().toString());
            oEstudiante.setStrCedula(edtCedula.getText().toString());
            oEstudiante.setIntEdad(Integer.parseInt(edtEdad.getText().toString()));
            json = oGson.toJson(oEstudiante);
            mensaje = oMMPE.modificarPerfil(json);
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            System.out.println(mensaje);
            if(!mensaje.equals("1")){
                Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();
                miIntent  = new Intent(modificarPerfilEstudiante.this, perfilEstudiante.class);
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
                startActivity(miIntent);
                finish();
            }else {
                Toast.makeText(getApplicationContext(), "Ocurrio algún error, inténtelo más tarde", Toast.LENGTH_LONG).show();
            }
        }


    }

//    private String convertImgToString(Bitmap bitmap) {
//        ByteArrayOutputStream array = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG,10,array);
//        byte[] imagenByte = array.toByteArray();
//        String imageString = Base64.encodeToString(imagenByte,Base64.DEFAULT);
//        return imageString;
//    }


}
