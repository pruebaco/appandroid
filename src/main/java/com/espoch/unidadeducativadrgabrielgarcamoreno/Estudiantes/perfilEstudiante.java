package com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import ggm.comun.estudiante;

import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class perfilEstudiante extends AppCompatActivity {
    Bundle oBundle;
    String json;
    estudiante oEstudiante = new estudiante();
    Gson oGson = new Gson();
    private EditText edtNombre;
    private EditText edtApelllido;
    private EditText edtCedula;
    private EditText edtEdad;
    private ImageView imgFotoEstudiantePerfil;
    //private String mensaje;
    Bitmap bmp;


    Intent miIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_estudiantes);
        edtNombre = findViewById(R.id.edtNombreEstudiante);
        edtApelllido = findViewById(R.id.edtApellidoEstudiante);
        edtCedula = findViewById(R.id.edtCedulaEstudiante);
        edtEdad = findViewById(R.id.edtEdadEstudiante);
        imgFotoEstudiantePerfil = findViewById(R.id.idFotoEstudiantePerfil);

        oBundle = this.getIntent().getExtras();
        if(oBundle!= null){
            json = oBundle.getString("json");
            oEstudiante = oGson.fromJson(json, estudiante.class);
            edtNombre.setText(oEstudiante.getStrNombres());
            edtApelllido.setText(oEstudiante.getStrApellidos());
            edtCedula.setText(oEstudiante.getStrCedula());
            edtEdad.setText(oEstudiante.getIntEdad().toString());
            Picasso.with(getApplicationContext()).load("http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/files/"+oEstudiante.getStrCedula()+".jpg").memoryPolicy(MemoryPolicy.NO_CACHE).error(R.mipmap.nodisponible).into(imgFotoEstudiantePerfil);

        } else {System.out.println("Nada we");}
    }

    public void modificarPerfilEstudiante(View v){
        miIntent  = new Intent(perfilEstudiante.this, modificarPerfilEstudiante.class);
        oBundle = new Bundle();
//        if(!mensaje.equals("")){
            oBundle.putString("json", json);
            miIntent.putExtras(oBundle);
        //}
        startActivity(miIntent);
        //System.out.println(mensaje);
        finish();
    }
}
