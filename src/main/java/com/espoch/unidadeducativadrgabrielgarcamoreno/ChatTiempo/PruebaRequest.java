package com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Blob;
//import java.util.Base64;

public class PruebaRequest extends AppCompatActivity {
    Intent miIntent;
    Uri path;
    Bitmap aux;
    Bitmap bitmap;
    ImageView imgFoto;
    byte[] oByte;
    String mensaje = "";
    String codificado = "";
    Blob bo;
    private EditText edtCedulaPrueba;

//    OkHttpClient client = new OkHttpClient();
//    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba_request);
        imgFoto = findViewById(R.id.imgPrueba);
        edtCedulaPrueba = findViewById(R.id.edtCedulaPrueba);
    }


//    String post(String url, String json) throws IOException {
//        RequestBody body = RequestBody.create(json, JSON);
//        Request request = new Request.Builder()
//                .url(url)
//                .post(body)
//                .build();
//        try (Response response = client.newCall(request).execute()) {
//            return response.body().string();
//        }
//    }


//Métodos para la carga de imágenes------------------------------------------------
    public void CargarImagenEstudiante(View view) {
        CargarImagenFuncion();
    }
    private void CargarImagenFuncion() {
        miIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        miIntent.setType("image/");
        startActivityForResult(miIntent.createChooser(miIntent, "Seleccione la aplicacion"), 10);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode ==RESULT_OK){
            path = data.getData();
            try {
                aux = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), path);
                imgFoto.setImageBitmap(aux);
                bitmap = ((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                oByte = baos.toByteArray();
//              codificado = codificar(oByte);
            } catch (IOException e) {
                e.printStackTrace();
            }
//imgFoto.setImageURI(path);
        }
    }

//Métodos para subir la imagen al servidor-----------------------------------------
    public void cargar(View v){
        Prueba oP = new Prueba();
        oP.execute();
    }
    class Prueba extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects) {
            mensaje = conexion(oByte, edtCedulaPrueba.getText().toString());
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2")){
                Toast.makeText(getApplicationContext(), "Datos: "+mensaje, Toast.LENGTH_LONG).show();
            }
        }
    }
    private String conexion(byte[] codifica, String cedula) {
        String wsdl_url = "http://10.0.2.2:8080/AppGgmGestion/sesionWS?WSDL";
        String soap_action = "http://sesion.ggm/subirImagen";
        String name_space = "http://sesion.ggm/";
        String method_name = "subirImagen";
        String resp;
        SoapObject soapObject = new SoapObject(name_space, method_name);
        //System.out.println("Archivo: "+codifica);
        soapObject.addProperty("bytImagen", codifica);
        soapObject.addProperty("cedula", cedula);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        new MarshalBase64().register(envelope);
        envelope.encodingStyle = SoapEnvelope.ENC;
//envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);
        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }

}
