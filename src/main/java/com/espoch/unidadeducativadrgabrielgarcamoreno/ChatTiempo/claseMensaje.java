package com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo;

public class claseMensaje {
    private String strId;
    private String strMensaje;
    private int tipoMensaje;
    private String strHorMensaje;

    public claseMensaje(){

    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStrMensaje() {
        return strMensaje;
    }

    public void setStrMensaje(String strMensaje) {
        this.strMensaje = strMensaje;
    }

    public int getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(int tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public String getStrHorMensaje() {
        return strHorMensaje;
    }

    public void setStrHorMensaje(String strHorMensaje) {
        this.strHorMensaje = strHorMensaje;
    }
}
