package com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import java.util.ArrayList;
import java.util.List;

public class adaptadorMensajes extends RecyclerView.Adapter<adaptadorMensajes.mensajesViewHolder> {

    private List<claseMensaje> listMensajes;

    public adaptadorMensajes(ArrayList<claseMensaje> listMensajes) {
        this.listMensajes = listMensajes;
    }

    @NonNull
    @Override
    public mensajesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_chatv1,viewGroup,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
                v.setLayoutParams(layoutParams);
        return new mensajesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull mensajesViewHolder mensajesViewHolder, int i) {
        RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams)mensajesViewHolder.cardView.getLayoutParams();
        FrameLayout.LayoutParams fl = (FrameLayout.LayoutParams)mensajesViewHolder.lnyMensaje.getLayoutParams();
        LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams)mensajesViewHolder.txvHora.getLayoutParams();
        LinearLayout.LayoutParams llMensaje = (LinearLayout.LayoutParams)mensajesViewHolder.txvMensaje.getLayoutParams();

        if(listMensajes.get(i).getTipoMensaje()==1){
            mensajesViewHolder.lnyMensaje.setBackgroundResource(R.mipmap.recibido);//EMISOR
            rl.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
            rl.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            fl.gravity = Gravity.LEFT;
            ll.gravity = Gravity.LEFT;
            llMensaje.gravity = Gravity.LEFT;
            mensajesViewHolder.txvMensaje.setGravity(Gravity.LEFT);
        }else if(listMensajes.get(i).getTipoMensaje()==2){
            mensajesViewHolder.lnyMensaje.setBackgroundResource(R.mipmap.enviado);//ENVIADO
            rl.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
            rl.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            fl.gravity = Gravity.RIGHT;
            ll.gravity = Gravity.RIGHT;
            llMensaje.gravity = Gravity.RIGHT;
            mensajesViewHolder.txvMensaje.setGravity(Gravity.RIGHT);
        }

        mensajesViewHolder.cardView.setLayoutParams(rl);
        mensajesViewHolder.lnyMensaje.setLayoutParams(fl);
        mensajesViewHolder.txvHora.setLayoutParams(ll);
        mensajesViewHolder.txvMensaje.setLayoutParams(llMensaje);

        mensajesViewHolder.txvMensaje.setText(listMensajes.get(i).getStrMensaje());
        mensajesViewHolder.txvHora.setText(listMensajes.get(i).getStrHorMensaje());

    }

    @Override
    public int getItemCount() {
        return listMensajes.size();
    }

    static class mensajesViewHolder extends RecyclerView.ViewHolder{
        LinearLayout lnyMensaje;
        CardView cardView;
        TextView txvMensaje;
        TextView txvHora;


        mensajesViewHolder(View itemView){
            super(itemView);
            lnyMensaje = itemView.findViewById(R.id.chatLinMensaje);
            cardView = itemView.findViewById(R.id.chatCard);
            txvMensaje = itemView.findViewById(R.id.chatTexto);
            txvHora = itemView.findViewById(R.id.chatHora);

        }

    }
}
