package com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Services.fireBaseId;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.ArrayList;
import java.util.List;

public class chatV1 extends AppCompatActivity {
    public static final String MENSAJE = "MENSAJE";
    public static final String TOKEN = "";

    private BroadcastReceiver oBroadcast;
    private RecyclerView rv;
    private ArrayList<claseMensaje> listMensajes;
    private adaptadorMensajes adapter;
    private Button btnEnviar;
    private EditText edtTexto;
    private int TEXT_LINES = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_v1);
        Toolbar toolbar = findViewById(R.id.chatTool);
        btnEnviar = findViewById(R.id.chatBtnAceptar);
        edtTexto = findViewById(R.id.chatEdtMensaje);

        rv = findViewById(R.id.chatRclv);
        listMensajes = new ArrayList<>();
        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        lm.setStackFromEnd(true);
        rv.setLayoutManager(lm);
        Intent intent = new Intent();

            String datos = getIntent().getStringExtra("datos");
            Integer actividad = getIntent().getIntExtra("actividad",0);
            Toast.makeText(getApplicationContext(),"En el chat la info es: "+datos+ " "+actividad, Toast.LENGTH_SHORT).show();
            edtTexto.setText(datos+" "+actividad);

        /*for(int i = 0;i<5;i++){
            claseMensaje mensajesAuxiliar = new claseMensaje();
            mensajesAuxiliar.setStrId(""+i);
            mensajesAuxiliar.setStrMensaje("Hola: "+i);
            mensajesAuxiliar.setTipoMensaje(1);
            mensajesAuxiliar.setStrHorMensaje("10:3"+i);
            listMensajes.add(mensajesAuxiliar);
        }
        for(int i = 0;i<5;i++){
            claseMensaje mensajesAuxiliar = new claseMensaje();
            mensajesAuxiliar.setStrId(""+i);
            mensajesAuxiliar.setStrMensaje("Hola receptor: "+i);
            mensajesAuxiliar.setTipoMensaje(2);
            mensajesAuxiliar.setStrHorMensaje("10:3"+i);
            listMensajes.add(mensajesAuxiliar);
        }

        adapter = new adaptadorMensajes(listMensajes);
        rv.setAdapter(adapter);*/

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        btnEnviar.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                String aux  = edtTexto.getText().toString();
//                //String TOKEN = getToken();
//                if(!aux.isEmpty()){
//                    crearMensaje(aux,2);
//                }
//
//            }
//        });


//        oBroadcast = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                Bundle oBundle = intent.getExtras();
////                String datos = oBundle.getString("datos");
////                Integer notificacion = oBundle.getInt("notificacion");
//
//                String datos = intent.getStringExtra("datos");
//                Integer notificacion = Integer.parseInt(intent.getStringExtra("notificacion"));
//
//
//                Toast.makeText(getApplicationContext(),"En el chat la info es: "+datos+""+notificacion, Toast.LENGTH_SHORT).show();
//                System.out.println("En el chat la info es: "+datos+" "+notificacion);
//                crearMensaje(datos,1);
//            }
//        };

       // LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(MENSAJE));



        //setScrollbarChat();
    }

    /*private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String datos = intent.getStringExtra("datos");
            Integer notificacion = Integer.parseInt(intent.getStringExtra("notificacion"));

            Toast.makeText(getApplicationContext(),"En el chat la info es: "+datos+""+notificacion, Toast.LENGTH_SHORT).show();
            System.out.println("En el chat la info es: "+datos+" "+notificacion);
        }
    };*/

    //DIFERENTE METODO-----------------------------------------------------------------------------
//    public void crearMensaje(String mensaje, int tipo){
//        claseMensaje mensajeAuxiliar = new claseMensaje();
//        mensajeAuxiliar.setStrId("0");
//        mensajeAuxiliar.setStrMensaje(mensaje);
//        mensajeAuxiliar.setTipoMensaje(tipo);
//        mensajeAuxiliar.setStrHorMensaje("10:3");
//        listMensajes.add(mensajeAuxiliar);
//        adapter.notifyDataSetChanged();
//        edtTexto.setText("");
//        setScrollbarChat();
//    }

//    public void setScrollbarChat(){
//        rv.scrollToPosition(adapter.getItemCount()-1);
//    }
//
//    public String getToken(){
//        final String[] token = {""};
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if(!task.isSuccessful()){
//                            Toast.makeText(getApplicationContext(),"Error al obtener Token",Toast.LENGTH_SHORT).show();
//                            return;
//                        }
//                        token[0] = task.getResult().getToken();
//                        String msg = "Su token obtenida es:"+ token[0];
//                        System.out.println(token[0]);
//                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//        return token[0];
//    }
//
//    @Override
//    protected void onPause(){
//        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(oBroadcast);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(oBroadcast,new IntentFilter(MENSAJE));
//    }
}
