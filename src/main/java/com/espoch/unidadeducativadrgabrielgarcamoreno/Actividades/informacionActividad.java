package com.espoch.unidadeducativadrgabrielgarcamoreno.Actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2.modificarActividad;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;

import ggm.comun.actividad;

public class informacionActividad extends AppCompatActivity {
    Bundle oBundle;
    Intent oIntent;
    private String json;
    private actividad oActividad;
    private Gson oGson;
    private EditText edtNombre;
    private EditText edtTipoActividad;
    private EditText edtDescripcion;
    private EditText edtFechaCreacion;
    private EditText edtFechaPlazo;
    private EditText edtCalificacion;
    private EditText edtDestinatariosActividad;
    private Spinner spiCalificacion;

    //private String [] vstrCalificacion;

    public informacionActividad(){
        json = "";
        oActividad = new actividad();
        oGson = new Gson();
        //vstrCalificacion = {"Hola"};
    }

//MÉTODO ON CREATE DEL ACTIVITY---------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_actividad);
        edtNombre = findViewById(R.id.edtNombreEstudianteI);
        edtTipoActividad = findViewById(R.id.edtTipoActividad);
        edtDescripcion = findViewById(R.id.edtDescripcionActividad);
        edtFechaCreacion = findViewById(R.id.edtFechaRealizadoActividad);
        edtFechaPlazo = findViewById(R.id.edtFechaPlazoActividad);
        edtCalificacion = findViewById(R.id.edtCalificacionActividad);
        edtDestinatariosActividad = findViewById(R.id.edtDestinatariosActividad);

    //Se obtiene los parámetros del anterior activity-----------------------------------------------
        oBundle = this.getIntent().getExtras();
        if(oBundle != null){
            json = oBundle.getString("json");
            oActividad = oGson.fromJson(json, actividad.class);
            edtNombre.setText(oActividad.getStrNombre());
            edtTipoActividad.setText(oActividad.getStrTipo());
            edtDescripcion.setText(oActividad.getStrDescripcion());
            edtFechaCreacion.setText(oActividad.getStrFechaRealizado());
            edtFechaPlazo.setText(oActividad.getStrFechaPlazo());
            edtDestinatariosActividad.setText(oActividad.getStrDestinatarios());
            System.out.println(oActividad.getStrNombre());
        }
    }

//MÉTODO PARA DETERMINAR LA NOTA CUALITATIVA--------------------------------------------------------
    public String setCalificacion(Integer cal){
        if (cal == 1){
            return "No entregado";
        } else if (cal == 2) {
            return "Incompleto";
        } else if (cal == 3){
            return "Entregado";
        } else return "Error";
    }

//MÉTODO PARA MODIFICAR LA ACTIVIDAD----------------------------------------------------------------
    public void modificarActividad(View v){
        oIntent = new Intent(informacionActividad.this, modificarActividad.class);
        oBundle = new Bundle();
        oBundle.putString("json", json);
        oIntent.putExtras(oBundle);
        startActivity(oIntent);
        finish();
    }
}
