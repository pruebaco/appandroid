package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Actividades.informacionActividad;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores.adaptadorListadoActividadesD;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.actividadMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import ggm.comun.actividad;

public class listadoActividades extends AppCompatActivity {
    private Bundle oBundle;
    private String json;
    private String mensaje;
    ArrayList<actividad> listaDatos;
    List<actividad> listaActividades;
    actividad oActividad;
    Gson oGson;
    actividadMO oMLAD;
    Intent myIntent;
    RecyclerView recycler;
    adaptadorListadoActividadesD oALAD;
    Switch swiFechas;

    public listadoActividades(){
        json = "";
        mensaje = "";
        oActividad = new actividad();
        oGson = new Gson();
        oMLAD = new actividadMO();
        listaDatos = new ArrayList<>();
    }

//MÉTODO ONCREATE DE LA ACTIVIDAD-------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_actividades_docente);
        swiFechas = findViewById(R.id.swiFechaActividadesDocente);
        swiFechas.setChecked(true);
        recycler = findViewById(R.id.rcvActividadesDocente);
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
        recycler.setHasFixedSize(true);

    //Obtención del parámetro activity anterior-----------------------------------------------------
        oBundle = this.getIntent().getExtras();
        if(oBundle != null){
            json = oBundle.getString("json");
            miLista ml = new miLista();
            ml.execute();
        }
    }

//CLASE PARA OBTENER LOS DATOS DEL SERVIDOR---------------------------------------------------------
    class miLista extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMLAD.listarActividadesDocente(json);
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                listaActividades = oGson.fromJson(mensaje, new TypeToken<List<actividad>>(){}.getType());
                if(listaActividades != null){
                    llenarActividades();
                    oALAD = new adaptadorListadoActividadesD(listaDatos);
                    oALAD.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            oActividad.setIntIdActividad(listaDatos.get(recycler.getChildAdapterPosition(v)).getIntIdActividad());
                            oActividad.setIntIdClase(listaDatos.get(recycler.getChildAdapterPosition(v)).getIntIdClase());
                            oActividad.setStrNombre(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrNombre());
                            oActividad.setStrFechaRealizado(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrFechaRealizado());
                            oActividad.setStrFechaPlazo(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrFechaPlazo());
                            oActividad.setStrDescripcion(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrDescripcion());
                            oActividad.setStrTipo(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrTipo());
                            oActividad.setStrDestinatarios(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrDestinatarios());
                            json = oGson.toJson(oActividad);
                            myIntent = new Intent(listadoActividades.this, informacionActividad.class);
                            oBundle = new Bundle();
                            oBundle.putString("json", json);
                            myIntent.putExtras(oBundle);
                            startActivity(myIntent);
                            //finish();
                        }
                    });
                    recycler.setAdapter(oALAD);
                }
            }
        }
    }

//MÉTODO PARA EL LLENADO DE ACTIVIDADES EN EL RECYCLER----------------------------------------------
    private void llenarActividades() {
        try{
            SimpleDateFormat oSDF = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
            oSDF.setTimeZone(TimeZone.getTimeZone("America/Guayaquil"));
            Date oDate = new Date();
            String strHora = oSDF.format(oDate);
            for(actividad object : listaActividades){
                if(swiFechas.isChecked()){
                    if(object.getStrFechaPlazo().compareTo(strHora) > 0){
                        swiFechas.setText("Actividades vigentes");
                        listaDatos.add(object);
                    }
                } else {
                    if(object.getStrFechaPlazo().compareTo(strHora) < 0){
                        swiFechas.setText("Actividades vencidas");
                        listaDatos.add(object);
                    }
                }
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error", Toast.LENGTH_LONG).show();
        }

    }

//MÉTODO PARA REFRESCAR EL SWITCH DE LAS FECHAS-----------------------------------------------------
    public void refrescarActividadesDocente(View view) {
        try{
            if(view.getId() == R.id.swiFechaActividadesDocente){
                listaDatos.clear();
                llenarActividades();
                oALAD.notifyDataSetChanged();
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error", Toast.LENGTH_LONG).show();
        }

    }

//MÉTODO PARA CREACIÓN DE NUEVA ACTIVIDAD-----------------------------------------------------------
    public void toNewActivity(View v){
        myIntent = new Intent(listadoActividades.this, ingresoActividad.class);
        oBundle.putString("json", json);
        myIntent.putExtras(oBundle);
        startActivity(myIntent);

        //finish();
    }

}
