package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Fragments.fragmentIngresoActividadDocente;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Fragments.fragmentNuevaActividadDocente;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.actividadMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ggm.comun.actividad;
import ggm.comun.materia;

public class ingresoActividad extends AppCompatActivity {

    private EditText edtNombre;
    private EditText edtDescripcion;
    private EditText edtFechaPlazo;
    private Spinner spiTipo;
    private Spinner spiMateria;
    private String mensaje;
    private List<materia> listaMaterias = new ArrayList<>();
    private Gson objetoGson = new Gson();


    actividadMO oMLAD = new actividadMO();

    Intent oIntent;
    Toolbar oToolbar;
    fragmentNuevaActividadDocente nuevaActividadDocentes;
    fragmentIngresoActividadDocente fragmentoActividadDocente;
    Bundle oBundle;
    String jsonPersona;
    List<String> listaAuxiliar = new ArrayList<>();
    List<Integer> listaIdAuxiliar = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_actividad_docente);

        nuevaActividadDocentes = new fragmentNuevaActividadDocente();
        fragmentoActividadDocente = new fragmentIngresoActividadDocente();

        oBundle = this.getIntent().getExtras();
        if(oBundle != null){
            jsonPersona = oBundle.getString("json", "");
        }

        edtNombre = findViewById(R.id.edtNombreActividadN);
        edtDescripcion = findViewById(R.id.edtDescripcionActividadN);
        edtFechaPlazo = findViewById(R.id.edtFechaPlazoActividadNu);
        spiTipo = findViewById(R.id.spiTipoActividadN);
        spiMateria = findViewById(R.id.spiCalificacionActividadN);
        String [] listaTiposActividad = {"Seleccione una opción", "-------------------------------------------", "Lección", "Tarea", "Evento", "Observación"};
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, R.layout.spinner_calificaciones_actividades, listaTiposActividad);
        spiTipo.setAdapter(adaptador);

        ListarMaterias LM = new ListarMaterias();
        LM.execute();

        oToolbar = findViewById(R.id.tbrModificar);
        oToolbar.setVisibility(View.VISIBLE);
        oToolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Datos de la actividad</font>"));
        setSupportActionBar(oToolbar);
        oToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);
    }

/*
* CARGA EL TOOLBAR CON LAS OPCIONES RESPECTIVAS DE LA ACTIVIDAD-------------------------------------
* */
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_modificar_perfil, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem){
        Integer aux = menuItem.getItemId();
        switch (aux){
            /*Opcion para enviar parámetros a la siguiente actividad #jsonPersona, #jsonActividad-*/
            case R.id.opGuardar:
                if(oBundle!= null){
                    actividad objetoActividad = new actividad();
                    Date date = new Date();
                    objetoActividad.setStrNombre(edtNombre.getText().toString());
                    if(listaIdAuxiliar.size() != 0){
                        objetoActividad.setIntIdClase(listaIdAuxiliar.get(spiMateria.getSelectedItemPosition()));
                    } else {
                        objetoActividad.setIntIdClase(0);
                    }
                    objetoActividad.setStrFechaRealizado((new Timestamp(date.getTime())).toString());
                    objetoActividad.setStrFechaPlazo(edtFechaPlazo.getText().toString());
                    objetoActividad.setStrDescripcion(edtDescripcion.getText().toString());
                    objetoActividad.setStrTipo(spiTipo.getSelectedItem().toString());
                    String jsonActividad = objetoGson.toJson(objetoActividad);
                    oIntent  = new Intent(ingresoActividad.this, listadoParalelos.class);
                    oBundle = new Bundle();
                    oBundle.putString("json", jsonPersona);
                    oBundle.putString("jsonActividad", jsonActividad);
                    oIntent.putExtras(oBundle);
                    startActivity(oIntent);

                }else {Toast.makeText(getApplicationContext(), "No", Toast.LENGTH_LONG).show();}
                break;
        }
        return true;
    }

    /*
    * CLASE PARA MOSTRAR LAS MATERIAS DE UN DOCENTE--------------------------------------------------
    * */
    class ListarMaterias extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMLAD.listarMateriasDocente(jsonPersona);
            System.out.println("Respuesta del servidor: "+mensaje);
            return null;
        }
        @Override protected void onPostExecute(Object o){
            try{
                if(mensaje != null && !mensaje.equals("2")){
                    listaMaterias = objetoGson.fromJson(mensaje, new TypeToken<List<materia>>(){}.getType());
                    if (listaMaterias != null){
                        for(materia m : listaMaterias){
                            listaAuxiliar.add(m.getStrNombreMAteria());
                            listaIdAuxiliar.add(m.getIntIdMAteria());
                        }
                        System.out.println("La Lista AQUI: "+listaAuxiliar);
                        llenarSpinner(listaAuxiliar);
                    }
                }
            }catch (Exception e){
                System.out.println("Error: ");
            }

        }
    }

    /*
    * CLASE PARA SOLICITAR LOS PARALELOS CONOCIENDO EL DOCENTE Y LAS MATERIA QUE IMPARTE
    * */

    /*
    * MÉTODO PARA LLENAR EL SPINNER DE MATERIAS
    * */
    public void llenarSpinner(List<String> lista){
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, R.layout.spinner_calificaciones_actividades, lista);
        spiMateria.setAdapter(adaptador);
    }

//INFLA EL MENU DEL TOOLBAR
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate(R.menu.menu_modificar_perfil, menu);
//        return true;
//    }

//    @Override
//    public void onFragmentInteraction(Uri uri) {
//
//    }
}
