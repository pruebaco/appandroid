package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import java.text.SimpleDateFormat;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores.adaptadorListadoParalelosD;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.actividadMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes.listadoEstudiantesDeParalelo;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

import ggm.comun.actividad;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.paralelo;

public class listadoParalelos extends AppCompatActivity {
    private Bundle oBundle;
    private String json = "";
    private String jsonActividad = null;
    private String mensaje = "";
    private String jsonAuxiliar = "";
    clase objetoClase = new clase();
    SimpleDateFormat so;
    actividad objetoActividad= new actividad();

    Intent myItent;
    actividadMO oMLPD = new actividadMO();

    RecyclerView recycler;

    ArrayList<paralelo> listaDatos;
    paralelo oParalelo = new paralelo();
    Gson objetoGson = new Gson();
    List<paralelo> listaParalelos;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_paralelos_docente);

        recycler = findViewById(R.id.rcvParalelosDocente);
        listaDatos = new ArrayList<>();
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
        recycler.setHasFixedSize(true);

        //Parámetro recibido de activity menu-------------------------------------------------------
        oBundle = this.getIntent().getExtras();
        if(oBundle!=null){
            json = oBundle.getString("json");
            jsonActividad = oBundle.getString("jsonActividad");
            docente objetoDocente = objetoGson.fromJson(json, docente.class);
            objetoActividad = objetoGson.fromJson(jsonActividad, actividad.class);

            objetoClase.setIntIdDocente(objetoDocente.getIntIdUsuario());
            objetoClase.setIntIdMateria(objetoActividad.getIntIdClase());

            //objetoActividad.setIntIdClase(null);

            //jsonActividad = objetoGson.toJson(objetoActividad);

            jsonAuxiliar = objetoGson.toJson(objetoClase);
            {
                Toast.makeText(this, jsonActividad+" Si ", Toast.LENGTH_LONG).show();}
                miLista ml = new miLista();
                ml.execute();                             //Aqui la llamada al metodo asíncrono-----

        }
        toolbar = findViewById(R.id.tbrModificar);
        toolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Seleccione el paralelo</font>"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);
    }

//CLASE PARA HACER LA CONEXIÓN CON EL SERVICIO WEB LISTAR PARALELOS DE UN DOCENTE-------------------

    /**ESTAMOS AQUI*/


    class miLista extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMLPD.listarParalelosForDocenteMateria(jsonAuxiliar);
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                System.out.println("Listado de: "+mensaje);
                listaParalelos = objetoGson.fromJson(mensaje, new TypeToken<List<paralelo>>(){}.getType());
                if(listaParalelos!= null){
                    llenarPersonajes();
                    adaptadorListadoParalelosD aPD = new adaptadorListadoParalelosD(listaDatos);
                    aPD.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            oParalelo.setStrParalelo(listaDatos.get(recycler.getChildAdapterPosition(v)).getStrParalelo());
                            oParalelo.setIntIdParalelo(listaDatos.get(recycler.getChildAdapterPosition(v)).getIntIdParalelo());

                            objetoClase.setIntIdParalelo(oParalelo.getIntIdParalelo());
                            jsonAuxiliar =objetoGson.toJson(objetoClase);

                            myItent = new Intent(listadoParalelos.this, listadoEstudiantesDeParalelo.class);
                            oBundle = new Bundle();
                            oBundle.putString("jsonClase", jsonAuxiliar);
                            oBundle.putString("jsonActividad", jsonActividad);
                            oBundle.putString("jsonParalelo", objetoGson.toJson(oParalelo));

                            //oBundle.putStringArrayList("lista", "[pedro]");

                            myItent.putExtras(oBundle);
                            startActivity(myItent);
                            finish();
                        }
                    });
                    recycler.setAdapter(aPD);
                }
            }
        }
}

    private void llenarPersonajes() {
        for(paralelo object : listaParalelos){
            listaDatos.add(object);
        }
    }


}
