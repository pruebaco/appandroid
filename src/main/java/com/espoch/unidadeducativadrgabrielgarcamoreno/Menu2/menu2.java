package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import ggm.comun.docente;

public class menu2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private String json;
    private Gson objetoGson;
    private docente objetoDocente;

    public menu2(){
        json = "";
        objetoGson = new Gson();
        objetoDocente = new docente();
    }

    private TextView texto;
    private TextView txvNombreUsuario;
    private Bundle oBundle;
    private Intent miIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//Boton flotante de la actividad------------------------------------------------------------
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        //Mensaje inicial en el menu de docente*******************
        //------------------------------------------------------------------------------------------
        texto = findViewById(R.id.txvPruebaDatos);




        oBundle = this.getIntent().getExtras();
        if(oBundle!=null){
            json = oBundle.getString("json");
            objetoDocente = objetoGson.fromJson(json, docente.class);

            texto.setText("Saludos: "+ objetoDocente.getStrNombre());

//**************************************************************************************************
        } else {texto.setText("Saludos: Docente");}

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


//Crea el menu en el toolbar lado derecho-----------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu2, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        txvNombreUsuario = findViewById(R.id.txvNombreUsuarioD);
        txvNombreUsuario.setText(objetoDocente.getStrNombre()+" "+objetoDocente.getStrApellido());
        int id = item.getItemId();

        if (id == R.id.navPerfilDocente) {
            // Handle the camera action
        } else if (id == R.id.navParalelosDocente) {
            miIntent = new Intent(menu2.this, listadoParalelos.class);
            if(!json.equals("")){
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
            }
            startActivity(miIntent);

        } else if (id == R.id.navActividadesDocente) {
            miIntent = new Intent(menu2.this, listadoActividades.class);
            if(!json.equals("")){
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
            }
            startActivity(miIntent);
        } else if (id == R.id.navAyudaDocente) {

        } else if (id == R.id.navSalirD) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;
    }
}
