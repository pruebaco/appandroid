package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.actividadMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.notificacionMO;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ggm.comun.actividad;
import ggm.comun.calificacion;
import ggm.comun.docente;
import ggm.comun.estudiante;
import ggm.comun.materia;
import ggm.comun.notificacion;
import ggm.comun.paralelo;

public class respuestaActividad extends AppCompatActivity {
    private String mensaje;
    private actividadMO objetoActividadMO;
    private notificacionMO objetoNotificacionMO;
    //private String jsonNotificacion;

    String jsonNotificacion = "";
    String jsonActividad = "";
    String jsonDocente= "";
    String jsonRepresentante = "";
    String jsonParalelo= "";
    String jsonMateria= "";
    String jsonEstudiante= "";
    String jsonCalificacion = "";

    private JsonObject objetoJson;

    EditText edtNombreActividad;
    EditText edtDescripcion;
    EditText edtFechaCreacion;
    EditText edtFechaPlazo;
    EditText edtNombreRemitente;
    EditText edtParalelo;
    EditText edtMateria;
    EditText edtEstudiante;
    EditText edtConversacion;

    Spinner spiCalificacion;
    Spinner spiTipo;

    actividad objetoActividad;
    Gson objetoGson;
    docente objetoDocente;
    paralelo objetoParalelo;
    materia objetoMateria;
    estudiante objetoEstudiante;
    notificacion objetoNotificacion;
    calificacion objetoCalificacion;

    public respuestaActividad(){
        mensaje = "";
        objetoActividadMO = new actividadMO();
//        jsonNotificacion = "";
        objetoJson = new JsonObject();
        objetoActividad = new actividad();
        objetoGson = new Gson();
        objetoDocente = new docente();
        objetoParalelo = new paralelo();
        objetoMateria = new materia();
        objetoEstudiante = new estudiante();
        objetoNotificacion = new notificacion();
        objetoCalificacion = new calificacion();
        objetoNotificacionMO = new notificacionMO();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respuesta_actividad);

        Toolbar objetoToolbar = findViewById(R.id.tbrResponderActividad);
        objetoToolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Información de la actividad</font>"));
        setSupportActionBar(objetoToolbar);
        objetoToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);

        edtNombreActividad = findViewById(R.id.edtNombreActividadM);
        edtDescripcion = findViewById(R.id.edtDescripcionActividadM);
        edtFechaCreacion = findViewById(R.id.edtFechaRealizadoActividadM);
        edtFechaPlazo = findViewById(R.id.edtFechaPlazoActividadM);
        edtNombreRemitente = findViewById(R.id.edtNombreDocenteM);
        edtParalelo = findViewById(R.id.edtParaleloM);
        edtMateria = findViewById(R.id.edtMateriaM);
        edtEstudiante = findViewById(R.id.edtNombreEstudianteM);
        edtConversacion = findViewById(R.id.edtRespuestaNotificacion);
        spiCalificacion = findViewById(R.id.spiCalificacionActividad);
        spiTipo = findViewById(R.id.spiTipoActividad);

        try{
            if(getIntent().getStringExtra("datos") != null){
                jsonNotificacion = getIntent().getStringExtra("datos");
                jsonActividad= getIntent().getStringExtra("actividad");
                jsonDocente= getIntent().getStringExtra("docente");
                jsonRepresentante = getIntent().getStringExtra("representante");
                jsonParalelo= getIntent().getStringExtra("paralelo");
                jsonMateria= getIntent().getStringExtra("materia");
                jsonEstudiante= getIntent().getStringExtra("estudiante");
                jsonCalificacion = getIntent().getStringExtra("calificacion");

                System.out.println("Datos json: "+jsonActividad+" Docente "+jsonDocente+" Paralelo "+jsonParalelo+" Materia "+jsonMateria+ " calificacion "+jsonCalificacion);

                objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
                objetoParalelo = objetoGson.fromJson(jsonParalelo, paralelo.class);
                objetoMateria = objetoGson.fromJson(jsonMateria, materia.class);
                objetoEstudiante = objetoGson.fromJson(jsonEstudiante, estudiante.class);
                objetoActividad = objetoGson.fromJson(jsonActividad, actividad.class);
                objetoNotificacion = objetoGson.fromJson(jsonNotificacion, notificacion.class);
                objetoCalificacion = objetoGson.fromJson(jsonCalificacion, calificacion.class);

                Toast.makeText(getApplicationContext(),"Datos: "+objetoDocente.getStrToken(), Toast.LENGTH_SHORT).show();

                edtNombreRemitente.setText(objetoDocente.getStrNombre()+" "+objetoDocente.getStrApellido());
                edtNombreRemitente.setEnabled(false);
                edtParalelo.setText(objetoParalelo.getStrParalelo());
                edtParalelo.setEnabled(false);
                edtMateria.setText(objetoMateria.getStrNombreMAteria());
                edtMateria.setEnabled(false);
                edtEstudiante.setText(objetoEstudiante.getStrNombres()+" "+objetoEstudiante.getStrApellidos());
                edtEstudiante.setEnabled(false);
                edtNombreActividad.setText(objetoActividad.getStrNombre());
                edtNombreActividad.setEnabled(false);
                edtDescripcion.setText(objetoActividad.getStrDescripcion());
                edtDescripcion.setEnabled(false);
                edtFechaCreacion.setText(objetoActividad.getStrFechaRealizado());
                edtFechaCreacion.setEnabled(false);
                edtFechaPlazo.setText(objetoActividad.getStrFechaPlazo());
                edtFechaPlazo.setEnabled(false);
                edtConversacion.setText(objetoNotificacion.getStrRespuesta());

                String [] strTipo = {"Tarea", "Leccion", "Reunion", "Evento"};
                ArrayAdapter<String> adaptador2 = new ArrayAdapter<String>(this, R.layout.spinner_calificaciones_actividades, strTipo);
                spiTipo.setAdapter(adaptador2);
                spiTipo.setSelection(tipoActividad());
                spiTipo.setEnabled(false);
                spiTipo.setClickable(false);

                String [] strCalificacion = {"No entregado", "Incompleto", "Completo"};
                ArrayAdapter<String> adaptador1 = new ArrayAdapter<String>(this, R.layout.spinner_calificaciones_actividades, strCalificacion);
                spiCalificacion.setAdapter(adaptador1);
                int aux=objetoCalificacion.getIntCalificacion();
                spiCalificacion.setSelection(aux);

                if(!objetoNotificacion.getBooEstadoDocente() && objetoNotificacion.getBooEstadoRepresentante()){
                    spiCalificacion.setEnabled(false);
                    spiCalificacion.setClickable(false);
                    objetoNotificacion.setBooEstadoDocente(true);
                    objetoNotificacion.setBooEstadoRepresentante(false);
                    System.out.println("ESTADOS BOOLEANS DE REMITENTE: "+objetoNotificacion.getBooEstadoDocente()+" "+objetoNotificacion.getBooEstadoRepresentante());
                }else if(objetoNotificacion.getBooEstadoDocente() && !objetoNotificacion.getBooEstadoRepresentante()){
                    objetoNotificacion.setBooEstadoDocente(false);
                    objetoNotificacion.setBooEstadoRepresentante(true);
                    System.out.println("ESTADOS BOOLEANS DE REMITENTE: "+objetoNotificacion.getBooEstadoDocente()+" "+objetoNotificacion.getBooEstadoRepresentante());
                }



            }
        }catch(Exception e){
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error inesperado", Toast.LENGTH_LONG).show();

        }

    }

//MÉTODO PARA DEVOLVER EL IDE DEL TIPO DE UNA ACTIVIDAD DE LA LISTA DE STRING GENERADA--------------
    public Integer tipoActividad(){
        int aux = 0;
        for(int i = 0; i<4;i++){
            if(spiTipo.getItemAtPosition(i).equals(objetoActividad.getStrTipo())){
                aux = i;
            }
        }
        objetoActividad.getStrTipo();
        return aux;
    }

//INFLADO DEL MENU TOOLBAR Y LLAMAR AL ENVIO DE LA ACTIVIDAD----------------------------------------
public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.menu_responder_actividad, menu);
    return true;
}
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Integer aux = menuItem.getItemId();
        switch (aux){
            case R.id.opResponder:
                objetoNotificacion.setStrRespuesta(edtConversacion.getText().toString());
                jsonNotificacion = objetoGson.toJson(objetoNotificacion);
                objetoCalificacion.setIntCalificacion((int) spiCalificacion.getSelectedItemId());
                jsonCalificacion = objetoGson.toJson(objetoCalificacion);

                getActividad objetoGetActividad = new getActividad();
                objetoGetActividad.execute();

                Toast.makeText(getApplicationContext(), "Respuesta: "+jsonNotificacion, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Calificacion: "+jsonCalificacion, Toast.LENGTH_LONG).show();
                break;
        }


        return true;
    }

//CLASE PARA OBTENER TODOS LOS DATOS DE LA ACTIVIDAD------------------------------------------------
    class getActividad extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            System.out.println("ANTES DE LA FUNCION");


            mensaje = objetoNotificacionMO.responderNotificacion(jsonNotificacion, jsonActividad, jsonDocente, jsonRepresentante, jsonParalelo, jsonMateria, jsonEstudiante, jsonCalificacion);
            System.out.println("DESPUES DE LA FUNCION");
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                //objetoJson = new JsonParser().parse(mensaje).getAsJsonObject();
                //edtNombreActividad.setText(mensaje);
                Toast.makeText(getApplicationContext(), "Respuesta positiva: "+mensaje, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Ocurrio algún error, inténtelo más tarde", Toast.LENGTH_LONG).show();
            }

        }
}


}
