package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Actividades.informacionActividad;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.modeloModificarActividadDocente;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;

import ggm.comun.actividad;

public class modificarActividad extends AppCompatActivity {
    Bundle oBundle;
    Intent oIntent;
    Toolbar oToolbar;
    private String mensaje;
    private String json;
    private Gson oGson;
    private modeloModificarActividadDocente oMMAD;
    private actividad oActividad;
    private EditText edtNombre;
    private EditText edtTipoActividad;
    private EditText edtDescripcion;
    private EditText edtFechaCreacion;
    private EditText edtFechaPlazo;
    private EditText edtCalificacion;
    private EditText edtDestinatarios;
    private Spinner spiTipoActividad;
    private Spinner spiCalificacion;

    public modificarActividad(){
        json = "";
        oGson = new Gson();
        oActividad = new actividad();
        mensaje = "";
        oMMAD = new modeloModificarActividadDocente();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_actividad);

        edtNombre = findViewById(R.id.edtNombreActividadM);
        edtDescripcion = findViewById(R.id.edtDescripcionActividadM);
        edtFechaCreacion = findViewById(R.id.edtFechaRealizadoActividadM);
        edtFechaPlazo = findViewById(R.id.edtFechaPlazoActividadM);
        //edtDestinatarios = findViewById(R.id.edtDestinatariosActividadM);
        spiTipoActividad = findViewById(R.id.spiTipoActividad);
        //spiCalificacion = findViewById(R.id.spiCalificacionActividad);

        //Se obtiene y se muestran los parámetros del anterior activity-----------------------------------------------
        try{
            oBundle = this.getIntent().getExtras();
            if(oBundle != null){
                json = oBundle.getString("json");
                oActividad = oGson.fromJson(json, actividad.class);
                edtNombre.setText(oActividad.getStrNombre());
                String aux2 = oActividad.getStrTipo();
                String [] strTipoActividad = {"Seleccione una opción", "-------------------------------------------", "Lección", "Deber", "Reunión", "Otro"};
                ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, R.layout.spinner_calificaciones_actividades, strTipoActividad);
                spiTipoActividad.setAdapter(adaptador);
                if(aux2.equals("Lección")){
                    spiTipoActividad.setSelection(2);

                } else if (aux2.equals("Deber")){
                    spiTipoActividad.setSelection(3);
                } else if (aux2.equals("Reunión")){
                    spiTipoActividad.setSelection(4);
                } else if (aux2.equals("Otro")){
                    spiTipoActividad.setSelection(5);
                }
                edtDescripcion.setText(oActividad.getStrDescripcion());
                edtFechaCreacion.setText(oActividad.getStrFechaRealizado());
                edtFechaPlazo.setText(oActividad.getStrFechaPlazo());
                Integer aux1 = 1;
                String [] strCalificacion = {"Seleccione una opción", "-------------------------------------------", "No entregado", "Incompleto", "Completo"};
                ArrayAdapter<String> adaptador1 = new ArrayAdapter<String>(this, R.layout.spinner_calificaciones_actividades, strCalificacion);
                spiCalificacion.setAdapter(adaptador1);
                if(aux1 != 0){
                    spiCalificacion.setSelection(aux1+1);
                }
                edtDestinatarios.setText(oActividad.getStrDestinatarios());
                System.out.println(oActividad.getStrNombre());
            }
        }catch(Exception e){
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
        }
        //Características del toolbar-------------------------------------------------------------------
        oToolbar = findViewById(R.id.tbrModificar);
oToolbar.setVisibility(View.VISIBLE);


        oToolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Modificar actividad</font>"));
        setSupportActionBar(oToolbar);
        oToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);

    }

    //INFLA EL MENÚ DEL TOOLBAR Y LLAMA LA FUNCION MODIFICAR----------------------------------------
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_modificar_perfil, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Integer aux = menuItem.getItemId();
        switch (aux){
            case R.id.opGuardar:
            //Envia un objeto actividad para modificar y responde satisfactorio o no------------
                if(oBundle != null){
                    Modificar oM = new Modificar();
                    oM.execute();

                } else {Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();
                    oIntent  = new Intent(modificarActividad.this, informacionActividad.class);
                    startActivity(oIntent);
                    finish();
                }
                break;
            case android.R.id.home:
                if(oBundle != null){
                    //oActividad.setStrCedula(edtCedula.getText().toString());
                    json = oGson.toJson(oActividad);
                    oIntent  = new Intent(modificarActividad.this, informacionActividad.class);
                    oBundle = new Bundle();

                    oBundle.putString("json", json);
                    oIntent.putExtras(oBundle);
                    startActivity(oIntent);
                    finish();
                }
                else {
                    oIntent  = new Intent(modificarActividad.this, informacionActividad.class);
                    startActivity(oIntent);
                    finish();
                }
                break;
        }
        return true;
    }

//MÉTODO PARA DETERMINAR LA NOTA CUALITATIVA--------------------------------------------------------
    public String setCalificacion(Integer cal){
        if (cal == 1){
            return "No entregado";
        } else if (cal == 2) {
            return "Incompleto";
        } else if (cal == 3){
            return "Entregado";
        } else return "Error";
    }

//CLASE PARA LA INTERACCIÓN CON EL SERVIDOR DE LA LÓGICA DE NEGOCIO---------------------------------
    class Modificar extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            System.out.println("IdClase "+oActividad.getIntIdClase());
            oActividad.setStrNombre(edtNombre.getText().toString());
            oActividad.setStrFechaPlazo(edtFechaPlazo.getText().toString());
            oActividad.setStrDescripcion(edtDescripcion.getText().toString());
            oActividad.setStrTipo((String) spiTipoActividad.getSelectedItem());
            json = oGson.toJson(oActividad);
            mensaje = oMMAD.modificarActividadDocente(json);
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(mensaje.equals("1")){
                Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();
                oIntent  = new Intent(modificarActividad.this, informacionActividad.class);
                oBundle = new Bundle();
                oBundle.putString("json", json);
                oIntent.putExtras(oBundle);
                startActivity(oIntent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Ocurrio algún error, inténtelo más tarde", Toast.LENGTH_LONG).show();
            }
        }

}

//    public void prueba(View v){
//        Toast.makeText(getApplicationContext(), "Bienvenido: "+spiTipoActividad.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
//    }

}
