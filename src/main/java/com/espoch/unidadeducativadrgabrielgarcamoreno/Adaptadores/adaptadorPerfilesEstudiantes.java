package com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import ggm.comun.estudiante;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;


public class adaptadorPerfilesEstudiantes extends RecyclerView.Adapter<adaptadorPerfilesEstudiantes.ViewHolderDatos>
        implements View.OnClickListener{
    //***************************************

    List<estudiante> listaPersonajes;

    private View.OnClickListener listener;
    byte[] blob;
    Bitmap bmp;


    public adaptadorPerfilesEstudiantes(ArrayList<estudiante> listaPersonajes) {
        this.listaPersonajes = listaPersonajes;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_perfil_estudiantes, viewGroup, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(layoutParams);
                //Para seleccionar segun la lista actual--------------------------------------------
                view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.txvNombre.setText(listaPersonajes.get(i).getStrNombres());
        viewHolderDatos.txvApellido.setText(listaPersonajes.get(i).getStrApellidos());
        //------------------------------------------------------------------------------------------
        //if(listaPersonajes.get(i).getStrFoto()!=null){
            Picasso.with(viewHolderDatos.itemView.getContext()).load("http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/files/"+listaPersonajes.get(i).getStrCedula()+".jpg")
                    .memoryPolicy(MemoryPolicy.NO_CACHE).error(R.mipmap.nodisponible).into(viewHolderDatos.imgFoto);
//            byte[] arr = Base64.decode(listaPersonajes.get(i).getStrFoto(), Base64.DEFAULT);
//            bmp = BitmapFactory.decodeByteArray(arr, 0, arr.length);
//            int alto = 60;
//            int ancho = 80;
//            if(bmp!= null){
//                viewHolderDatos.imgFoto.setImageBitmap(Bitmap.createScaledBitmap(bmp,alto, ancho, true));
//                //viewHolderDatos.imgFoto.setImageBitmap(bmp);
//            } else {
//                System.out.println("No cargó csm");
//            }
        //}









    }

    @Override
    public int getItemCount() {
        return listaPersonajes.size();
    }


    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;
    }

    @Override
    public void onClick(View v) {
        if(listener!= null){
            listener.onClick(v);
        }
    }


//Clase main para cargar datos----------------------------------------------------------------------
    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView txvNombre, txvApellido;
        ImageView imgFoto;
        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            txvNombre = itemView.findViewById(R.id.txvNombreList);
            txvApellido = itemView.findViewById(R.id.txvParalelo);
            imgFoto = itemView.findViewById(R.id.idImagenOpcEst);
        }
    }

//    ArrayList<String> listaDatos;
//
//    public adaptadorPerfilesEstudiantes(ArrayList<String> listaDatos) {
//        this.listaDatos = listaDatos;
//    }
//
//    @NonNull
//    @Override
//    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_perfil_estudiantes,null,false);
//        return new ViewHolderDatos(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
//        viewHolderDatos.asignarDatos(listaDatos.get(i));
//    }
//
//    @Override
//    public int getItemCount() {
//        return listaDatos.size();
//    }
//
//    public class ViewHolderDatos extends RecyclerView.ViewHolder {
//
//        TextView dato;
//
//        public ViewHolderDatos(@NonNull View itemView) {
//            super(itemView);
//            dato = itemView.findViewById(R.id.msj1);
//        }
//
//        public void asignarDatos(String s) {
//            dato.setText(s);
//        }
//    }
//******************************************************************************************************

}
