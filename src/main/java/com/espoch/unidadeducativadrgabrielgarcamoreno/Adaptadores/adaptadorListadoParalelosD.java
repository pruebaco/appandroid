package com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.paralelo;

public class adaptadorListadoParalelosD
    extends RecyclerView.Adapter<adaptadorListadoParalelosD.ViewHolderParalelos>
    implements View.OnClickListener {

    List<paralelo> listaParalelos;
    private View.OnClickListener listener;
    byte[] blob;
    Bitmap bmp;

    public adaptadorListadoParalelosD(ArrayList<paralelo> listaParalelo){
        this.listaParalelos = listaParalelo;
    }

    @NonNull
    @Override
    public ViewHolderParalelos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_paralelos_docente, viewGroup, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(layoutParams);

                view.setOnClickListener(this);
        return new ViewHolderParalelos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderParalelos viewHolderDatos, int i) {
        //Toast.makeText(adaptadorListadoParalelosD.this, "No tiene acceso a internet", Toast.LENGTH_LONG).show();
        //viewHolderDatos.txvAno.setText(listaParalelos.get(i).getStrAno());
        viewHolderDatos.txvParalelo.setText(listaParalelos.get(i).getStrParalelo());
    }

    @Override
    public int getItemCount() {return listaParalelos.size();}

    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }

    }


//CLASE MAIN PARA CARGAR LOS DATOS--------------------------------------------------------
    public class ViewHolderParalelos extends RecyclerView.ViewHolder{
        TextView txvParalelo;
    //txvAno,
        public ViewHolderParalelos(@NonNull View itemView){
            super(itemView);
            //txvAno = itemView.findViewById(R.id.txvAno);
            txvParalelo = itemView.findViewById(R.id.txvParaleloL);
        }
    }

}
