package com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.estudiante;

public class adaptadorListadoEstudiantesParalelo extends RecyclerView.Adapter<adaptadorListadoEstudiantesParalelo.MyViewHolder>{
    private Context context;
    private List<estudiante> list;
    private List<Integer> selectedIds = new ArrayList<>();

    public adaptadorListadoEstudiantesParalelo(Context context, List<estudiante> list){
        this.context = context;
        this.list = list;
        System.out.println(list);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_estudiantes_paralelo, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      holder.title.setText(list.get(position).getStrNombres());
      holder.apellido.setText(list.get(position).getStrApellidos());
      holder.cedula.setText(list.get(position).getStrCedula());
        //holder.title.setText("Prueba");
        int id = list.get(position).getIntIdEstudiante();
        if (selectedIds.contains(id)){
            //if item is selected then,set foreground color of FrameLayout.
            holder.rootView.setForeground(new ColorDrawable(ContextCompat.getColor(context,R.color.presionado)));
        }
        else {
            //else remove selected item color.
            holder.rootView.setForeground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public estudiante getItem(int position){
        return list.get(position);
    }

    public void setSelectedIds(List<Integer> selectedIds) {
        this.selectedIds = selectedIds;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title, apellido, cedula;
        FrameLayout rootView;
        MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv);
            apellido = itemView.findViewById(R.id.txvApellidoEstudianteL);
            cedula = itemView.findViewById(R.id.txvCedulaEstudianteL);
            rootView = itemView.findViewById(R.id.root_view);
        }
    }
}
