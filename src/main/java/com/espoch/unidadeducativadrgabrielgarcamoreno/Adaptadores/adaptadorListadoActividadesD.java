package com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.actividad;

public class adaptadorListadoActividadesD
    extends RecyclerView.Adapter<adaptadorListadoActividadesD.ViewHolderActividades>
    implements View.OnClickListener{

    List<actividad> listaActividades;
    private View.OnClickListener listener;

    public adaptadorListadoActividadesD(ArrayList<actividad> listaPersonajes){
        this.listaActividades = listaPersonajes;
    }

//MÉTODOS NECESARIO DE LA CLASE PARA EL RECYCLERVIEW------------------------------------------------
    @NonNull
    @Override
    public ViewHolderActividades onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_actividades_docente, viewGroup, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(layoutParams);
            view.setOnClickListener(this);
        return new ViewHolderActividades(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderActividades viewHolderActividades, int i) {
            viewHolderActividades.txvNombre.setText(listaActividades.get(i).getStrNombre());
            viewHolderActividades.txvFechaPlazo.setText(listaActividades.get(i).getStrFechaPlazo());
    }

    @Override
    public int getItemCount() {
        return listaActividades.size();
    }

//MÉTODO NECESARIO PARA SELECCIONAR ALGUNO DEL RECYCLERVIEW-----------------------------------------
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;
    }

    @Override
    public void onClick(View v){
        if(listener != null){
            listener.onClick(v);
        }
    }

//CLASE MAIN DEL ADAPTADOR--------------------------------------------------------------------------
    public class ViewHolderActividades extends RecyclerView.ViewHolder{
        TextView txvNombre, txvFechaPlazo;
        public ViewHolderActividades(@NonNull View itemView){
            super(itemView);
                txvNombre = itemView.findViewById(R.id.txvNombreActividad);
                txvFechaPlazo = itemView.findViewById(R.id.txvFechaPlazoActividad);
        }
    }
}
