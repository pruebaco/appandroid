package com.espoch.unidadeducativadrgabrielgarcamoreno.Adaptadores;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import java.util.ArrayList;
import ggm.comun.materia;

public class adaptadorListadoMaterias extends RecyclerView.Adapter<adaptadorListadoMaterias.MateriasViewHolder>
        implements View.OnClickListener {

    ArrayList<materia> listaMaterias;
    private View.OnClickListener listener;



    public adaptadorListadoMaterias(ArrayList<materia> listaMateriasN){
        this.listaMaterias = listaMateriasN;
    }

    @NonNull
    @Override
    public MateriasViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_materias_docente, null,false);

//        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
//        view.setLayoutParams(layoutParams);

        view.setOnClickListener(this);
        return new MateriasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MateriasViewHolder materiasViewHolder, int i) {
        materiasViewHolder.txtNombre.setText(listaMaterias.get(i).getStrNombreMAteria());
    }

    @Override
    public int getItemCount() {
        return listaMaterias.size();
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    public class MateriasViewHolder extends RecyclerView.ViewHolder{
        TextView txtNombre;

        public MateriasViewHolder(View itemView){
            super(itemView);
            txtNombre = itemView.findViewById(R.id.txvNombreMateria);
        }
    }

}
