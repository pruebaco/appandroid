package com.espoch.unidadeducativadrgabrielgarcamoreno.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.espoch.unidadeducativadrgabrielgarcamoreno.R;

import java.util.ArrayList;
import java.util.List;

import ggm.comun.materia;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragmentNuevaActividadDocente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragmentNuevaActividadDocente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragmentNuevaActividadDocente extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ArrayList<materia> listaMaterias;
    private RecyclerView rcvListadoMaterias;
    private EditText edtNombre;
    private EditText edtDescripcion;
    private EditText edtFechaPlazo;
    private EditText edtTipo;
    private Spinner spiMateria;
    private Bundle oBundle;
    private String mensaje = "";
    private String datoAnterior;
    private Integer numero;

    public fragmentNuevaActividadDocente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragmentNuevaActividadDocente.
     */
    // TODO: Rename and change types and number of parameters
    public static fragmentNuevaActividadDocente newInstance(String param1, String param2) {
        fragmentNuevaActividadDocente fragment = new fragmentNuevaActividadDocente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            datoAnterior = getArguments().getString("Prueba", "nope");
            numero = getArguments().getInt("simon",0);
            System.out.println(datoAnterior+"   "+numero);

        }

        oBundle = new Bundle();
        oBundle.putString("string", "hey");
        fragmentIngresoActividadDocente obj = new fragmentIngresoActividadDocente();
        obj.setArguments(oBundle);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_nueva_actividad_docente, container, false);
        listaMaterias = new ArrayList<>();
        edtNombre = vista.findViewById(R.id.edtNombreActividadN);
        edtDescripcion = vista.findViewById(R.id.edtDescripcionActividadN);
        edtFechaPlazo = vista.findViewById(R.id.edtFechaPlazoActividadNu);
        //edtTipo = vista.findViewById(R.id.edtTipoActividadN);
        spiMateria=vista.findViewById(R.id.spiCalificacionActividadN);
        try{
            String causa = "";
            //objetoBundle =
        }catch(Exception e){
            e.getMessage();
        }

//        List<String> strMaterias = new ArrayList<>();
//        strMaterias.add("Hola");
//        strMaterias.add("Como");
//        strMaterias.add("Estas");
//        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getContext(), R.layout.spinner_calificaciones_actividades, strMaterias);
//        spiMateria.setAdapter(adaptador);
//        rcvListadoMaterias = vista.findViewById(R.id.rcvListadoMaterias);
//        rcvListadoMaterias.setLayoutManager(new LinearLayoutManager(getContext()));
//
//        llenarListaMaterias();
//        adaptadorListadoMaterias oALM = new adaptadorListadoMaterias(listaMaterias);
//        rcvListadoMaterias.setAdapter(oALM);
//        oALM.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getContext(), "Selecciono: "+listaMaterias.get(rcvListadoMaterias
//                        .getChildAdapterPosition(v)).getStrNombreMAteria(), Toast.LENGTH_SHORT).show();
//            }
//        });

        return vista;
    }

//    class ListarMaterias extends AsyncTask{
//        @Override
//        protected Object doInBackground(Object[] objects){
//
//            mensaje = "";
//        }
//    }

    private void llenarListaMaterias() {
        materia aux = new materia();
        aux.setStrNombreMAteria("Juan");
        listaMaterias.add(aux);
        listaMaterias.add(aux);
        listaMaterias.add(aux);
        listaMaterias.add(aux);
        listaMaterias.add(aux);
        listaMaterias.add(aux);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
