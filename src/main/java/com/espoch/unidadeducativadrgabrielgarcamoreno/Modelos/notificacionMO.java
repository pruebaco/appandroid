package com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class notificacionMO {

    public String responderNotificacion(String strNotificacion, String strActividad, String strDocente, String strRepresentante, String strParalelo, String strMateria, String strEstudiante, String strCalificacion){
        System.out.println("EN LA FUNCION");
        System.out.println(strNotificacion+" % "+strActividad+" % "+strDocente+" % "+strRepresentante+" % "+strParalelo+" % "+strMateria+" % "+strEstudiante+" % "+strCalificacion);
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/actividadWS?WSDL";
        String soap_action = "http://serviciosweb/retroalimentacionActividad";
        String name_space = "http://serviciosweb/";
        String method_name = "retroalimentacionActividad";
        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("jsonNotificacion",strNotificacion);
        soapObject.addProperty("jsonActividad",strActividad);
        soapObject.addProperty("jsonDocente",strDocente);
        soapObject.addProperty("jsonRepresentante",strRepresentante);
        soapObject.addProperty("jsonParalelo",strParalelo);
        soapObject.addProperty("jsonMateria",strMateria);
        soapObject.addProperty("jsonEstudiante",strEstudiante);
        soapObject.addProperty("jsonCalificacion",strCalificacion);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//      envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);
        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
        //return "vale";
    }

}
