package com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class actividadMO {
    public String listarActividadesDocente(String json2) {
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/docenteWS?WSDL";
        String soap_action = "http://serviciosweb/listarActividadesFechaU";
        String name_space = "http://serviciosweb/";
        String method_name = "listarActividadesFechaU";
        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("json",json2);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//      envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);
        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }



    public String listarMateriasDocente(String json2){
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/docenteWS?WSDL";
        String soap_action = "http://serviciosweb/ListarMateriasDocente";
        String name_space = "http://serviciosweb/";
        String method_name = "ListarMateriasDocente";
        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("json",json2);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//      envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);
        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }

    public String listarParalelosForDocenteMateria(String jsonClase) {
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/docenteWS?WSDL";
        String soap_action = "http://serviciosweb/listarParalelosDocenteMateria";
        String name_space = "http://serviciosweb/";
        String method_name = "listarParalelosDocenteMateria";

        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("jsonClase",jsonClase);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);

        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }

//CLIENTE PARA MOSTRAR LA ACTIVIDAD CON SU DOCENTE, MATERIA Y ESTUDIANTE----------------------------
    /*public String responderActividad(String jsonNotificacion, String jsonActividad, String jsonDocente, String jsonRepresentante, String jsonParalelo, String jsonMateria, String jsonEstudiante, String jsonCalificacion){
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/docenteWS?WSDL";
        String soap_action = "http://serviciosweb/listarParalelosDocenteMateria";
        String name_space = "http://serviciosweb/";
        String method_name = "listarParalelosDocenteMateria";

        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("jsonNotificacion",jsonNotificacion);
        soapObject.addProperty("jsonActividad",jsonActividad);
        soapObject.addProperty("jsonDocente",jsonDocente);
        soapObject.addProperty("jsonRepresentante",jsonRepresentante);
        soapObject.addProperty("jsonParalelo",jsonParalelo);
        soapObject.addProperty("jsonMateria",jsonMateria);
        soapObject.addProperty("jsonEstudiante",jsonEstudiante);
        soapObject.addProperty("jsonCalificacion",jsonCalificacion);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);

        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }*/
}
