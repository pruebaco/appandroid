package com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class modeloModificarActividadDocente {
    public String modificarActividadDocente (String json2){
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/actividadWS?WSDL";
        String soap_action = "http://actividad.ggm/modificarActividad";
        String name_space = "http://actividad.ggm/";
        String method_name = "modificarActividad";
        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("json",json2);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);

        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }
}
