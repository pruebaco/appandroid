package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.modeloPerfilRepresentante;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import ggm.comun.representante;

public class perfilRepresentante extends AppCompatActivity {
    Toolbar toolbar;
    private EditText edtNombre;
    private EditText edtApellido;
    private EditText edtCedula;
    private EditText edtEdad;
    private EditText edtCorreo;
    private EditText edtTelefono;
    private EditText edtCelular1;
    private EditText edtNivelAca;
    private EditText edtDireccion;
    private ImageView imgFotoRepresentante;
    private String json = "";
    private String mensaje = "";
    byte[] blob;
    Context context;
    Bitmap bmp;

    representante oRepresentante = new representante();
    modeloPerfilRepresentante omPR = new modeloPerfilRepresentante();
    Gson oGson = new Gson();
    Bundle oBundle;
    Intent miIntent;


//METODO ONCREATE MAIN---------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_representante);
        edtNombre = findViewById(R.id.edtNombre);
        edtApellido = findViewById(R.id.edtApellido);
        edtCedula = findViewById(R.id.edtCedula);
        edtEdad = findViewById(R.id.edtEdad);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtTelefono = findViewById(R.id.edtTelefono);
        edtCelular1 = findViewById(R.id.edtCelular1);
        edtNivelAca = findViewById(R.id.edtNivelAca);
        edtDireccion = findViewById(R.id.edtDireccion);
        imgFotoRepresentante = findViewById(R.id.idFotoRepresentante);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Modificar perfil</font>"));
        setSupportActionBar(toolbar);

        //Parámetro recibido del anterior activity---------------------------
        oBundle = this.getIntent().getExtras();
        if (oBundle!= null){
            json = oBundle.getString("json");
            MyTask mt = new MyTask();
            mt.execute();
        }
        //-----------------------------------------------------------------
//        edtNombre.setText("Holi");
//
//
//        Bundle oBundle = this.getIntent().getExtras();
//        json = oBundle.getString("json");
//        if(oBundle!= null){
//            oRepresentante = oGson.fromJson(json, representante.class);
//            edtApellido.setText("Saludos: "+oRepresentante.getStrNombre());
//        }

//        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//        boolean hayConexion = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
//        if (hayConexion){
//            MyTask mt = new MyTask();
//            mt.execute();
//        } else {
//            Toast.makeText(getApplicationContext(), "No tiene acceso a internet", Toast.LENGTH_LONG).show();
//        }
    }
//    public void mostrarPerfil (View v){
//
//    }

//CLASE PARA HACER LA CONEXIÓN CON EL SERVICIO WEB, CONEXIÓN CON EL MODELO--------------
    class MyTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects){
            //Envía un objeto usuario y recibe un representante-------------
            mensaje = omPR.perfil(json);
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2") && !mensaje.equals("-1")){
                oRepresentante = oGson.fromJson(mensaje, representante.class);
                edtNombre.setText(oRepresentante.getStrNombre());
                edtApellido.setText(oRepresentante.getStrApellido());
                edtCedula.setText(oRepresentante.getStrCedula());
                edtEdad.setText(oRepresentante.getIntEdad().toString());
                edtCorreo.setText(oRepresentante.getStrCorreo());
                edtTelefono.setText(oRepresentante.getStrTelefono());
                edtCelular1.setText(oRepresentante.getStrCelular1());
                edtNivelAca.setText(oRepresentante.getStrNivelAca());
                edtDireccion.setText(oRepresentante.getStrDireccion());
                Picasso.with(getApplicationContext()).load("http://10.0.2.2:8080/AppGgmGestion/files/"+oRepresentante.getStrCedula()+".jpg").error(R.mipmap.nodisponible).memoryPolicy(MemoryPolicy.NO_CACHE).into(imgFotoRepresentante);
//                byte[] arr = Base64.decode(oRepresentante.getStrFoto(), Base64.DEFAULT);
//                bmp = BitmapFactory.decodeByteArray(arr,0,arr.length);
//                int alto = 120;
//                int ancho = 160;
//                if(bmp!= null){
//                    imgFotoRepresentante.setImageBitmap(Bitmap.createScaledBitmap(bmp,alto,ancho,true));
//                }
                //System.out.println("Aqui: "+arr);
                //****************************************************
                //bmp = ByteToImage(arr);
//                int alto = 100;
//                int ancho = 100;
//                if(bmp!= null){
//                    imgFotoRepresentante.setImageBitmap(Bitmap.createScaledBitmap(bmp,alto,ancho,true));
//                } else {System.out.println("No disponible");}

                Toast.makeText(getApplicationContext(), "Bienvenido: "+oRepresentante.getStrNombre()+" "+oRepresentante.getStrApellido(), Toast.LENGTH_LONG).show();
            } else if(mensaje.equals("-1")){
                Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();

            } else {Toast.makeText(getApplicationContext(), "Servidor desconectado", Toast.LENGTH_SHORT).show();}
            super.onPostExecute(o);
        }
    }



    //MÉTODO QUE INVOCA LA MODIFICACIÓN DEL REPRESENTANTE-----------------------------------------------
    public void modificarPerfil (View v){
        miIntent  = new Intent(perfilRepresentante.this, modificarRepresentante.class);
        oBundle = new Bundle();
        if(!mensaje.equals("")){
            oBundle.putString("json", mensaje);
            miIntent.putExtras(oBundle);
        }
        startActivity(miIntent);
        System.out.println(mensaje);
        finish();
    }

}
