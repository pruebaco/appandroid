package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.espoch.unidadeducativadrgabrielgarcamoreno.Modelos.modeloModificarRepresentante;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import ggm.comun.representante;
import ggm.comun.usuario;

public class modificarRepresentante extends AppCompatActivity {
    Toolbar toolbar;

    private EditText edtNombre;
    private EditText edtApellido;
    private EditText edtCedula;
    private EditText edtEdad;
    private EditText edtCorreo;
    private EditText edtTelefono;
    private EditText edtCelular1;
    private EditText edtNivelAca;
    private EditText edtDireccion;
    private ImageView imgFoto;
    //*********************
    Uri path;
    //*********************
    InputStream oIn;
    //private Button btnCargarFoto;
    private String json = "";
    private String mensaje;
    representante oRepresentante = new representante();
    modeloModificarRepresentante oMMR = new modeloModificarRepresentante();
    usuario oUsuario = new usuario();
    Gson oGson = new Gson();
    Intent miIntent;
    Bundle oBundle;
    Bitmap bitmap;
    Bitmap aux;
    Bitmap bmp;
    byte[] oByte;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_representante);
        edtNombre = findViewById(R.id.edtNombre);
        edtApellido = findViewById(R.id.edtApellido);
        edtCedula = findViewById(R.id.edtCedula);
        edtEdad = findViewById(R.id.edtEdad);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtTelefono = findViewById(R.id.edtTelefono);
        edtCelular1 = findViewById(R.id.edtCelular1);
        edtNivelAca = findViewById(R.id.edtNivelAca);
        edtDireccion = findViewById(R.id.edtDireccion);
        imgFoto = findViewById(R.id.imgFotoRepresentanteM);

        //Recibe el parámetro de la anterior activity------------------------------
        try{
            oBundle = this.getIntent().getExtras();
            if(oBundle!=null){
                json = oBundle.getString("json");
                oRepresentante = oGson.fromJson(json, representante.class);
                edtNombre.setText(oRepresentante.getStrNombre());
                edtApellido.setText(oRepresentante.getStrApellido());
                edtCedula.setText(oRepresentante.getStrCedula());
                edtEdad.setText(oRepresentante.getIntEdad().toString());
                edtCorreo.setText(oRepresentante.getStrCorreo());
                edtTelefono.setText(oRepresentante.getStrTelefono());
                edtCelular1.setText(oRepresentante.getStrCelular1());
                edtNivelAca.setText(oRepresentante.getStrNivelAca());
                edtDireccion.setText(oRepresentante.getStrDireccion());
                Picasso.with(getApplicationContext()).load("http://10.0.2.2:8080/AppGgmGestion/files/"+oRepresentante.getStrCedula()+".jpg").error(R.mipmap.nodisponible).memoryPolicy(MemoryPolicy.NO_CACHE).into(imgFoto);
            }
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(), "Servidor desconectado", Toast.LENGTH_SHORT).show();
        }

    //Características del toolbar-------------------------------------------------------------------
        toolbar = findViewById(R.id.tbrModificar);
        toolbar.setTitle(Html.fromHtml("<font color='#ffffff'>Modificar perfil</font>"));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_10dp);
    }

//INFLA EL MENÚ DEL TOOLBAR-------------------------------------------------------------------------
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_modificar_perfil, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem menuItem){
        Integer aux = menuItem.getItemId();
        switch (aux){
            case R.id.opGuardar:
                //Envia un objeto representante para modificar y responde satisfactorio o no
                if(oBundle != null){
                    Modificar oM = new Modificar();
                    oM.execute();

                } else {Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();

                    miIntent  = new Intent(modificarRepresentante.this, perfilRepresentante.class);
                    startActivity(miIntent);
                    finish();
                }
                break;
            case android.R.id.home:
                if(oBundle != null){
                    oUsuario.setStrCedula(edtCedula.getText().toString());
                    json = oGson.toJson(oUsuario);
                    miIntent  = new Intent(modificarRepresentante.this, perfilRepresentante.class);
                    oBundle = new Bundle();

                    oBundle.putString("json", json);
                    miIntent.putExtras(oBundle);
                    startActivity(miIntent);
                    finish();
                }
                else {
                    miIntent  = new Intent(modificarRepresentante.this, perfilRepresentante.class);
                    startActivity(miIntent);
                    finish();
                }
                break;
        }
        return true;
    }

    //Métodos para cargar una imagen y colocarla en el imageview------------------------------------
    public void CargarImagen(View view) {
        CargarImagenFuncion();
    }
    private void CargarImagenFuncion() {
        miIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        miIntent.setType("image/");
        startActivityForResult(miIntent.createChooser(miIntent, "Seleccione la aplicacion"), 10);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode ==RESULT_OK){
            path = data.getData();
            try {
                aux = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), path);
                imgFoto.setImageBitmap(aux);
                bitmap = ((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                oByte = baos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


//Método para subir la imagen al servidor-----------------------------------------------------------
    public void subirImagenBoton(View v){
        subirImagen sI = new subirImagen();
        sI.execute();
    }
    class subirImagen extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            mensaje = oMMR.subirImagen(oByte, edtCedula.getText().toString());
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(!mensaje.equals("-2")){
                Toast.makeText(getApplicationContext(), "Datos: "+mensaje, Toast.LENGTH_LONG).show();
            }
        }
    }




    class Modificar extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects){
            oRepresentante.setStrNombre(edtNombre.getText().toString());
            oRepresentante.setStrApellido(edtApellido.getText().toString());
            oRepresentante.setStrCedula(edtCedula.getText().toString());
            oRepresentante.setIntEdad(Integer.parseInt(edtEdad.getText().toString()));
            oRepresentante.setStrCorreo(edtCorreo.getText().toString());
            oRepresentante.setStrTelefono(edtTelefono.getText().toString());
            oRepresentante.setStrCelular1(edtCelular1.getText().toString());
            oRepresentante.setStrNivelAca(edtNivelAca.getText().toString());
            oRepresentante.setStrDireccion(edtDireccion.getText().toString());
            //La foto seleccionada se lo convierte en String base64--------------------
            //try {
                //aux = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), path);
//                aux = ((BitmapDrawable)imgFoto.getDrawable()).getBitmap();
//                String strFotoCon;
//                strFotoCon = convertImgToString(aux);
//                oRepresentante.setStrFoto(strFotoCon);
//                System.out.println("Prueba: "+strFotoCon);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            json = oGson.toJson(oRepresentante);
            mensaje = oMMR.modificarPerfil(json);
            return null;
        }
        @Override
        protected void onPostExecute(Object o){
            if(mensaje.equals("1")){
                oUsuario.setStrCedula(edtCedula.getText().toString());
                json = oGson.toJson(oUsuario);
                Toast.makeText(getApplicationContext(), "Cambios realizados", Toast.LENGTH_LONG).show();
                miIntent  = new Intent(modificarRepresentante.this, perfilRepresentante.class);
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
                startActivity(miIntent);
                finish();

            } else {
                Toast.makeText(getApplicationContext(), "Ocurrio algún error, inténtelo más tarde", Toast.LENGTH_LONG).show();
            }
        }

    }

//    private String convertImgToString(Bitmap bitmap) {
//        ByteArrayOutputStream array = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG,20,array);
//        byte[] imagenByte = array.toByteArray();
//        String imageString = Base64.encodeToString(imagenByte,Base64.DEFAULT);
//        return imageString;
//    }
}
