package com.espoch.unidadeducativadrgabrielgarcamoreno.Menu1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;
import com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo.PruebaRequest;
import com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo.chatV1;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Estudiantes.opcionesPerfilesEstudiantes;
import com.google.gson.Gson;
import ggm.comun.representante;

public class Menu1 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
        private representante objetoRepresentante;
        private Gson objetoGson;
        private String json;

        private TextView texto;
        private TextView txvNombreUsuario;
        private Bundle oBundle;
        private Intent miIntent;

        public Menu1(){
            objetoRepresentante = new representante();
            objetoGson = new Gson();
            json = "";
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //Mensaje inicial en el menu de representantes*********************************************
        //-----------------------------------------------------------------------------------------
        texto = findViewById(R.id.txvPruebaDatos);
        oBundle = this.getIntent().getExtras();
        if(oBundle!=null){
            json = oBundle.getString("json");
            objetoRepresentante = objetoGson.fromJson(json, representante.class);
            texto.setText("Saludos: "+ objetoRepresentante.getStrNombre());
//**************************************************************************************************
        } else {texto.setText("Saludos: Representante");}

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        txvNombreUsuario = findViewById(R.id.txvNombreUsuarioR);
        txvNombreUsuario.setText(objetoRepresentante.getStrNombre()+" "+objetoRepresentante.getStrApellido());
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//OPCIONES DEL MENU DE REPRESENTANTE----------------------------------------------------------------
        if (id == R.id.navPerfilDocente) {
            miIntent  = new Intent(Menu1.this, perfilRepresentante.class);
//**************************************************************************************************
            if(!json.equals("")){
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
            }
            startActivity(miIntent);


        } else if (id == R.id.navParalelosDocente) {
            miIntent  = new Intent(Menu1.this, opcionesPerfilesEstudiantes.class);
//**************************************************************************************************
            if(!json.equals("")){
                oBundle = new Bundle();
                oBundle.putString("json", json);
                miIntent.putExtras(oBundle);
            }
//            Toast.makeText(getApplicationContext(), "Segundo", Toast.LENGTH_LONG).show();
            startActivity(miIntent);

        } else if (id == R.id.navActividadesDocente) {
            miIntent = new Intent(Menu1.this, PruebaRequest.class);
            startActivity(miIntent);
        } else if (id == R.id.navAyudaDocente) {
            miIntent = new Intent(Menu1.this, chatV1.class);
            startActivity(miIntent);
        } else if (id == R.id.navSalirD) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
