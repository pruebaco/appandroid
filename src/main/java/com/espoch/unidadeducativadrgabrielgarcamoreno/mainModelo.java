package com.espoch.unidadeducativadrgabrielgarcamoreno;

//import org.json.simple.parser.JSONParser;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.LOCAL_HOST;
import static com.espoch.unidadeducativadrgabrielgarcamoreno.MainActivity.PUERTO;

public class mainModelo {
    public String strToken = "";

    public String loadData(String json2){
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/sesionWS?WSDL";
        String soap_action = "http://serviciosweb/VerificarCuenta";
        String name_space = "http://serviciosweb/";
        String method_name = "VerificarCuenta";
        String resp, mensaje;

        SoapObject soapObject = new SoapObject(name_space, method_name);
        soapObject.addProperty("json",json2);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);
        HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);

        try{
            htse.call(soap_action, envelope);
            SoapObject obj = (SoapObject) envelope.bodyIn;
            resp = obj.getProperty(0).toString();
            System.out.println(resp);
            mensaje = resp;
        }catch(Exception e){
            e.printStackTrace();
            mensaje = "-2";
        }
        return mensaje;
    }

    public String loadToken(String json, String strToken){                      //Obtenemos el token del dispositivo movil
        //String mensaje = "";
        String wsdl_url = "http://"+LOCAL_HOST+":"+PUERTO+"/AppGgmGestion/sesionWS?WSDL";
        String soap_action = "http://serviciosweb/subirToken";
        String name_space = "http://serviciosweb/";
        String method_name = "subirToken";
        String resp ="";

        try{
            String result = "";
                JSONObject Jobj = new JSONObject(json);
                String strCedula = Jobj.getString("strCedula");            //Obtenemos la cedula ingresada por el usuario
                JSONObject obj2 = new JSONObject();
                obj2.put("strCedula",strCedula);
                obj2.put("strToken", strToken);
                result = obj2.toString();                                       //Los datos cedula y token convertidos en un json
                System.out.println(result);

                SoapObject soapObject = new SoapObject(name_space, method_name);
                soapObject.addProperty("json", result);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.setOutputSoapObject(soapObject);
                HttpTransportSE htse = new HttpTransportSE(wsdl_url, 1000);

                htse.call(soap_action, envelope);
                SoapObject obj = (SoapObject) envelope.bodyIn;
                resp = obj.getProperty(0).toString();
                System.out.println(resp);
            return resp;
        }catch(Exception e){
            e.printStackTrace();
            return "-2";
        }
    }
}
