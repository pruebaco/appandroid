package com.espoch.unidadeducativadrgabrielgarcamoreno.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.espoch.unidadeducativadrgabrielgarcamoreno.ChatTiempo.chatV1;
import com.espoch.unidadeducativadrgabrielgarcamoreno.Menu2.respuestaActividad;
import com.espoch.unidadeducativadrgabrielgarcamoreno.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Random;

import ggm.comun.notificacion;

import static android.support.v4.app.NotificationCompat.*;

public class fireBaseMensajes extends FirebaseMessagingService {
    String datos="";
    Gson objetoGson = new Gson();
    //Integer notificacion = 0;
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String TAG = "Información";
        Log.d(TAG, "Mensaje con datos "+remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if(remoteMessage.getData() != null){
            String strNotificacion = remoteMessage.getData().get("notificacion");
            String strActividad = remoteMessage.getData().get("actividad");
            String strDocente = remoteMessage.getData().get("docente");
            String strRepresentante = remoteMessage.getData().get("representante");
            String strParalelo = remoteMessage.getData().get("paralelo");
            String strMateria = remoteMessage.getData().get("materia");
            String strEstudiante = remoteMessage.getData().get("estudiante");
            String strCalificacion = remoteMessage.getData().get("calificacion");

            //Integer intIdActividad = Integer.parseInt(remoteMessage.getData().get("idActividad"));
            String strEmisor = remoteMessage.getFrom();
            Log.d(TAG, "Mensaje recibido de: "+strEmisor);
            //notificacion(strTexto, intIdNotificacion);

            mostrarNotificacion(strNotificacion, strActividad, strDocente, strRepresentante, strParalelo, strMateria, strEstudiante, strCalificacion);
            Log.d(TAG, "Notificacion: "+strNotificacion);
        }else Log.d(TAG, "Mensaje sin datos ");

    }

    /*private void notificacion(String datos2, Integer idNotificacion2){
        Intent oIntent = new Intent(chatV1.MENSAJE);
        oIntent.putExtra("datos",datos2);
        oIntent.putExtra("notificacion", idNotificacion2.toString());

//        objetoBundle.putString("datos",datos2);
//        objetoBundle.putInt("notificacion", idNotificacion2);
//
//        oIntent.putExtras(objetoBundle);

//        startActivity(oIntent);

        //getApplicationContext().sendBroadcast(oIntent);

        //LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(oIntent);
    }*/

    private void mostrarNotificacion(String jsonNotificacion, String jsonActividad, String jsonDocente, String jsonRepresentante, String jsonParalelo, String jsonMateria, String jsonEstudiante, String jsonCalificaion){
        Intent oIntent = new Intent(this, respuestaActividad.class);
        oIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        oIntent.putExtra("datos",jsonNotificacion);

        notificacion objetoNotificacion =  objetoGson.fromJson(jsonNotificacion, notificacion.class);

        oIntent.putExtra("actividad",jsonActividad);
        oIntent.putExtra("docente",jsonDocente);
        oIntent.putExtra("representante", jsonRepresentante);
        oIntent.putExtra("paralelo",jsonParalelo);
        oIntent.putExtra("materia",jsonMateria);
        oIntent.putExtra("estudiante", jsonEstudiante);
        oIntent.putExtra("calificacion", jsonCalificaion);


        //oIntent.putExtra("actividad", idActividad);

        //PendingIntent pendingIntent = PendingIntent.getActivity(this,0, oIntent,PendingIntent.FLAG_ONE_SHOT);
        //PendingIntent pendingIntent = PendingIntent.getActivity(this,0, oIntent,PendingIntent.FLAG_UPDATE_CURRENT );

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0, oIntent, PendingIntent.FLAG_ONE_SHOT);
        Builder oBuilder = new Builder(this, "Chanel1");
        Uri sounNotification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        oBuilder.setAutoCancel(true);
        oBuilder.setContentTitle("Sagamo");
        oBuilder.setContentText(objetoNotificacion.getStrNombreNotificacion());
        oBuilder.setSound(sounNotification);

        //oBuilder.setSmallIcon(R.drawable.ic_account_circle_white_24dp);
        //oBuilder.setLargeIcon(R.mipmap.imgnotificacion);
        //oBuilder.setSmallIcon(R.drawable.ic_school_black);

        oBuilder.setSmallIcon(R.mipmap.imgnotificacion);
        oBuilder.setTicker("Nueva notificacion de Sagamo");
        oBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Random numero = new Random();
        notificationManager.notify(numero.nextInt(), oBuilder.build());
    }
}
